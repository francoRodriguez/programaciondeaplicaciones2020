package datatypes;

import java.util.Date;

public class DtUsuario {
	private String nickName;
	private String nombre;
	private String apellido;
	private String correo;
	private Date fechaNacimiento;
	private boolean esDocente;
	
	public DtUsuario(String nickName, String nombre, String apellido, String correo, Date fechaNacimiento, boolean esDocente) {
		super();
		this.nickName = nickName;
		this.nombre = nombre;
		this.apellido = apellido;
		this.correo = correo;
		this.fechaNacimiento = fechaNacimiento;
		this.esDocente = esDocente;
	}
	
	public String getNickName() {
		return this.nickName;
	}
	public String getNombre() {
		return this.nombre;
	}
	public String getApellido() {
		return this.apellido;
	}
	public String getCorreo() {
		return this.correo;
	}
	public Date getFechaNacimiento() {
		return this.fechaNacimiento;
	}
	public boolean getEsDocente() {
		return this.esDocente;
	}
	
	@Override
	public String toString() {
		return "NICKNAME = " + nickName + "\nNOMBRE = " + nombre + "\nAPELLIDO = " + apellido + "\nCORREO = " + correo + "\nFECHA DE NACIMIENTO = " + fechaNacimiento + "\nES DOCENTE = " + esDocente;
	}
	
}
