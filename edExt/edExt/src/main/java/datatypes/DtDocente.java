package datatypes;

import java.util.Date;

public class DtDocente extends DtUsuario {
	String nombreInsituto;
	
	public DtDocente(String nickName, String nombre, String apellido, String correo, Date fechaNacimiento, Boolean esDocente, String nombreInstituto) {
		super (nickName, nombre, apellido, correo, fechaNacimiento, esDocente);
		this.nombreInsituto= nombreInstituto ;
	}

	public String getNombreInstituto() {
		return this.nombreInsituto;
	}

	@Override
	public String toString() {
		return super.toString() + "\nINSTITUTO = " + nombreInsituto;
	}

}