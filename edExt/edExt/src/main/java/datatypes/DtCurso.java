package datatypes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import logica.Curso;
import logica.Edicion;

@SuppressWarnings("unused")
public class DtCurso {
	private String nombre;
	private String descripcion;
	private String duracion;
	private int cantidadHoras;
	private int cantidadCreditos;	
	private String url;
	private Date fechaAlta;
	public DtCurso(String nombre, String descripcion, String duracion, int cantidadHoras, int cantidadCreditos,
			String url, Date fechaAlta) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.duracion = duracion;
		this.cantidadHoras = cantidadHoras;
		this.cantidadCreditos = cantidadCreditos;
		this.url = url;
		this.fechaAlta = fechaAlta;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDuracion() {
		return duracion;
	}
	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}
	public int getCantidadHoras() {
		return cantidadHoras;
	}
	public void setCantidadHoras(int cantidadHoras) {
		this.cantidadHoras = cantidadHoras;
	}
	public int getCantidadCreditos() {
		return cantidadCreditos;
	}
	public void setCantidadCreditos(int cantidadCreditos) {
		this.cantidadCreditos = cantidadCreditos;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public DtCurso() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
