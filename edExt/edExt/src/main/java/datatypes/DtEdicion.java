package datatypes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import logica.Docente;
import logica.Usuario;

@SuppressWarnings("unused")
public class DtEdicion {
	private String nombre;
	private Date fechaI;
	private Date fechaF;
	private int cupo;
	private Date fechaPub;
	private List<Docente> listaDocente = new ArrayList<Docente>();
	public DtEdicion() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DtEdicion(String nombre, Date fechaI, Date fechaF, int cupo, Date fechaPub, List<Docente> listaDocente) {
		super();
		this.nombre = nombre;
		this.fechaI = fechaI;
		this.fechaF = fechaF;
		this.cupo = cupo;
		this.fechaPub = fechaPub;
		this.listaDocente = listaDocente;
	}
	public String getNombre() {
		return nombre;
	}
	public Date getFechaI() {
		return fechaI;
	}
	public Date getFechaF() {
		return fechaF;
	}
	public int getCupo() {
		return cupo;
	}
	public Date getFechaPub() {
		return fechaPub;
	}
	public List<Docente> getListaDocentes() {
		return listaDocente;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setFechaI(Date fechaI) {
		this.fechaI = fechaI;
	}
	public void setFechaF(Date fechaF) {
		this.fechaF = fechaF;
	}
	public void setCupo(int cupo) {
		this.cupo = cupo;
	}
	public void setFechaPub(Date fechaPub) {
		this.fechaPub = fechaPub;
	}
	public void setListaDocente(List<Docente> listaDocente) {
		this.listaDocente = listaDocente;
	}
	@Override
	public String toString() {
		return nombre +" " + cupo +"Fi " + fechaI ;
	}

	

}
