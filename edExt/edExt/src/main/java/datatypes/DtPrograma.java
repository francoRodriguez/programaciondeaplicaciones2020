package datatypes;

import java.util.Date;

public class DtPrograma {
	private String nombre;
	private String descripcion;
	private Date fechaI;
	private Date fechaF;
	public DtPrograma(String nombre, String descripcion, Date fechaI, Date fechaF) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fechaI = fechaI;
		this.fechaF = fechaF;
	}
	public DtPrograma() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getNombre() {
		return nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public Date getFechaI() {
		return fechaI;
	}
	public Date getFechaF() {
		return fechaF;
	}
	
}
