package logica;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import datatypes.DtCurso;
import datatypes.DtEdicion;
import persistencia.Conexion;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;


@SuppressWarnings("unused")


@Entity
//@Table(name="CURSOS")
public class Curso {
	@Id
	private String nombre;
	private String descripcion;
	private String duracion;
	private int cantidadHoras;
	private int cantidadCreditos;	
	private String url;
	private Date fechaAlta;
	
	
	
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	private List<Edicion> ediciones = new ArrayList<Edicion>();
	
	@ManyToMany(cascade=CascadeType.ALL)
	private List<Curso> previas = new ArrayList<Curso>(); //el public es para las pruebas

	public Curso() {
		super();
		// TODO Auto-generated constructor stub
		System.out.println("Se creo CURSO ");
		//Edicion(String nombre, Date fechaI, Date fechaF, int cupo, Date fechaPub)
		//Date dt = new Date();

	}
	public Curso(String nombre, String descripcion, String duracion, int cantidadHoras, int cantidadCreditos, String url,Date fechaAlta) {
		super();
		this.setNombre(nombre);
		this.setDescripcion(descripcion);
		this.setDuracion(duracion);
		this.setCantidadHoras(cantidadHoras);
		this.setCantidadCreditos(cantidadCreditos);
		this.setUrl(url);
		this.setFechaAlta(fechaAlta);
		//this.setEdiciones(ediciones);
		
		//System.out.println("Se creo CURSO "+ nombre );
		//Edicion e1= new Edicion("PrimeaEdicion", new Date(),new Date(), 45,new Date());
		//Edicion e2= new Edicion("SegundaEdicion", new Date(),new Date(), 45,new Date());
		//ediciones.add(e1); //ediciones.add(e2);


	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDuracion() {
		return duracion;
	}
	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}
	public int getCantidadHoras() {
		return cantidadHoras;
	}
	public void setCantidadHoras(int cantidadHoras) {
		this.cantidadHoras = cantidadHoras;
	}
	public int getCantidadCreditos() {
		return cantidadCreditos;
	}
	public void setCantidadCreditos(int cantidadCreditos) {
		this.cantidadCreditos = cantidadCreditos;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	
	public List<Edicion> getEdiciones() {
		return ediciones;
	}
	public void setEdiciones(List<Edicion> ediciones) {
		this.ediciones = ediciones;
	}
	public void addPRevia(Curso cur) {
		//Conexion conexion = Conexion.getInstancia();
		//EntityManager em = conexion.getEntityManager();
		//em.getTransaction().begin();

		this.getPrevias().add(cur);
		//em.persist(this);
		//em.getTransaction().commit();
	}
	public List<Curso> getPrevias() {
		return previas;
	}
	public DtCurso getDtCurso() {
		DtCurso dtc = new DtCurso();
		dtc.setNombre(this.nombre);;
		dtc.setDescripcion(this.descripcion);
		dtc.setDuracion(this.duracion);
		dtc.setCantidadCreditos(this.cantidadCreditos);
		dtc.setCantidadHoras(this.cantidadHoras);
		dtc.setUrl(this.url);
		dtc.setFechaAlta(this.fechaAlta);
		return dtc;
	}
	public List<String> obtenerEdiciones(){
		List<String> lista = new ArrayList<String>();
		for(Edicion ed: ediciones) {
			lista.add(ed.getNombre());
		}
		return lista;
	}
	public DtEdicion obtenerUnaEdicion(String nombreEdicion) {
		DtEdicion dte = new DtEdicion();
		for(Edicion ed: ediciones) {
			if(ed.getNombre().equals(nombreEdicion)) {
				dte=ed.getDtEdicion();
			}
		}
		return dte;
	}
	
	public boolean existeEdicion(String nombreEdicion) {
		System.out.println("evaluo: "+nombreEdicion);
		for(Edicion ed: ediciones) {
			System.out.println("enbase: "+ed.getNombre());
			if(ed.getNombre().equals(nombreEdicion)) {
				return true;
			}
		}	
		return false;
	}
	
	public void addEdicion(Edicion e) {
		ediciones.add(e);		
	}

}
