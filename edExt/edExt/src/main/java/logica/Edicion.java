package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import datatypes.DtEdicion;
import persistencia.Conexion;



@Entity
public class Edicion {	
	@Id
	private String nombre;
	private Date fechaI;
	private Date fechaF;
	private int cupo;
	private Date fechaPub;
	
	@ManyToMany(cascade= CascadeType.ALL)
	private List<Docente> listaDocente= new ArrayList<Docente>();
	
	
	@OneToMany (mappedBy= "edicion",cascade= CascadeType.ALL,orphanRemoval=true)
	private List<InscripcionEdc> inscripciones = new ArrayList<>();
	
	public Edicion() {
		super();
	}
	
	public Edicion(String nombre, Date fechaI, Date fechaF, int cupo, Date fechaPub, List<Docente> listaDocente) {

		System.out.println("Se creo Edicion: " + nombre);
		this.setNombre(nombre);
		this.setFechaI(fechaI);
		this.setFechaF(fechaF);
		this.setCupo(cupo);
		this.setFechaPub(fechaPub);	
		this.setListaDocente(listaDocente);
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Date getFechaI() {
		return fechaI;
	}
	public void setFechaI(Date fechaI) {
		this.fechaI = fechaI;
	}
	public Date getFechaF() {
		return fechaF;
	}
	public void setFechaF(Date fechaF) {
		this.fechaF = fechaF;
	}
	public int getCupo() {
		return cupo;
	}
	public void setCupo(int cupo) {
		this.cupo = cupo;
	}
	public Date getFechaPub() {
		return fechaPub;
	}
	public void setFechaPub(Date fechaPub) {
		this.fechaPub = fechaPub;
	}
	public List<Docente> getListaDocente() {
		return listaDocente;
	}
	public void setListaDocente(List<Docente> listaDocente) {
		this.listaDocente = listaDocente;
	}
	
	DtEdicion getDtEdicion() {
		return new DtEdicion(this.nombre, this.fechaI, this.fechaF,this.cupo,this.fechaPub, this.listaDocente);
	}
	public void agregarInscripcion(Usuario usuario) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		em.getTransaction().begin();
		//em.persist(u);
		Date factual = new Date();
		InscripcionEdc i = new InscripcionEdc(factual,usuario,this);
		inscripciones.add(i);
		usuario.getInscripciones().add(i);
		em.getTransaction().commit();
	}


}
