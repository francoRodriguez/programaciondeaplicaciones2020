package logica;

import java.util.ArrayList;


import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Query;

import datatypes.DtCurso;
import datatypes.DtEdicion;
import persistencia.Conexion;




@SuppressWarnings("unused")
@Entity
public class Instituto {
	@Id
	private String nombre;
	@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	private List<Curso> cursos= new ArrayList<Curso>();
	public  static Curso c; // el public es para testear
	public Instituto() {
		super();
		//Date d1 = new Date();
		//Curso c1 = new Curso("PAV8","Materia", "1", 78, 25 ,"www.blablaPAP.com",d1);// p probar
		//Curso c2 = new Curso("PAV3","Materia", "1", 78, 25 ,"www.blablaPAV.com",d1);// p probar
		//cursos.add(c1); cursos.add(c2);

	}
	public Instituto(String nombre) {
		this.setNombre(nombre);	
		this.setCursos(cursos);
	}
	public String getNombre() {
		return this.nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<Curso> getCursos() {
		return cursos;
	}
	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}
	
	public void add(Curso c) {	//se va a precisar
		cursos.add(c);
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		em.getTransaction().begin();
		
		em.persist(c);
		System.out.print("CCCCCCCCCCUUUUURRRRSSSSSOOOOOO");
		
		em.getTransaction().commit();
	}
	
	public boolean noExisteCurso( String nombre ) {
		boolean retorno = true;
		for(Curso c:cursos) {
			System.out.println("Listando cursos en noexiste " + c.getNombre());
			if(c.getNombre().equals(nombre)){
				retorno = false;
			}
		}
		return retorno;
	}
	public Curso obtenerCurso(String nombre) {
		Curso retorno = null;
		for(Curso cur:cursos) {
			if(cur.getNombre().equals(nombre)){
				retorno = cur;
			}
		}
		if(retorno==null) System.out.println("Institutos.obtenercursos es null");
		return retorno;
	}
	
	public void setearPrevias(List<String> previas) {
		for(String s: previas) {
			c.addPRevia(this.obtenerCurso(s));
		}
	}
	public void crearCursoStatic(String nombre) { // hay que agregarlo despues a la lista
		c = new Curso();
		c.setNombre("todavia sin nombre");
	}
	public void setearCurso (String nombreCurso , String desc ,String duracion ,int cantHoras,int  cantCreditos, String url, Date fecha ) {
		c.setNombre(nombreCurso);c.setDescripcion(desc);c.setDuracion(duracion);
		c.setCantidadHoras(cantHoras);c.setCantidadCreditos(cantCreditos);
		c.setUrl(url);c.setFechaAlta(fecha);
	}
	public List<String> listarNombreCursos() {	
		List<String> salida = new ArrayList<String>();
		for(Curso k: this.cursos) {
			salida.add(k.getNombre());
			System.out.println( this.nombre + " " + k.getNombre());
		} 	
		return salida;
	}
	public void confirmarAltaCurso() {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		em.getTransaction().begin();
		//Curso nuev = new Curso(c.getNombre(),c.getDescripcion(),c.getDuracion(), c.getCantidadHoras(), c.getCantidadCreditos(),c.getUrl(), c.getFechaAlta());
		this.getCursos().add(c);
		System.out.println("Se agrego el curso "+ c.getNombre() + " a "+ this.getNombre());
		for(Curso cur:cursos) System.out.println(cur.getNombre());
		//em.persist(this);
		em.getTransaction().commit();
		//c=null;// borro la memoria
	}

	public DtCurso mostrarDatosdeCurso(String nombreCurso) {
		DtCurso dtc =null;
		Curso cur = this.obtenerCurso(nombreCurso);
		if(cur != null) dtc = cur.getDtCurso();
		return dtc;		
	}
	public List<String> obtenerEdiciones(String nommbreCurso){
		c= this.obtenerCurso(nommbreCurso);
		return c.obtenerEdiciones();
	}
	public DtEdicion obtenerEdicion(String nombreEdicion) {
		return c.obtenerUnaEdicion(nombreEdicion);
	}
	public void borrarC() {
		c=null;
	}

	
}
