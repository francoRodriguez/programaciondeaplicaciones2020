package logica;
import logica.Docente;
import logica.Usuario;
import persistencia.Conexion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import datatypes.DtEdicion;



@SuppressWarnings("unused")
public class ManejadorUsuario {
	private static ManejadorUsuario instancia = null;
	private List<Usuario> usuarios = new ArrayList<Usuario>();
	
	public ManejadorUsuario(){}
	
	public static ManejadorUsuario getInstancia() {
		if (instancia == null)
			instancia = new ManejadorUsuario();
		return instancia;
	}
	
	//AgregarUsuairio
	public void add(Usuario u) {
		
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		em.getTransaction().begin();
		em.persist(u);
		em.getTransaction().commit();
		
		//usuarios.add(u);
	}
	
	//BuscarUsuario
	public Usuario existeUsuario(String nickName) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Usuario usuario = em.find(Usuario.class, nickName);
		return usuario;
	}
		
	
	//BuscarEmail devuelve un string con el mail encontrado, si no encuentra devuelve null
	public String existeEmail(String email) {
		String aretornar = null;
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Usuario usuario = em.find(Usuario.class, email);
		if (usuario != null) {
				aretornar=usuario.getCorreo();
			}
		return aretornar;
	}
	
	// devuelve una lista de string de nikname de docentes	
	public List<String> getDocentes() {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Query query = em.createQuery("select u from Usuario u");
		List<Usuario> docentes = query.getResultList();
		List<String> salida = new ArrayList<String>();
		for(Usuario d: docentes) {	
			if (d.getEsDocente()) {
				salida.add(d.getNickName());			
			}				
		} 
		return salida;	
	}
	public List<String> getEstudiantes() {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Query query = em.createQuery("select u from Usuario u");
		List<Usuario> docentes = query.getResultList();
		List<String> salida = new ArrayList<String>();
		for(Usuario d: docentes) {	
			if (!d.getEsDocente()) {
				salida.add(d.getNickName());			
			}				
		} 
		return salida;	
	}
	
	public void altaUsuarioEstudiante(String nickName, String nombre, String apellido, String correo, Date fechaNacimiento, Boolean esDocente) {
		
		Estudiante e = new Estudiante();
		e.setNickName(nickName);
		e.setNombre(nombre);
		e.setApellido(apellido);
		e.setCorreo(correo);
		e.setFechaNacimiento(fechaNacimiento);
		e.setEsDocente(esDocente);
		usuarios.add(e);
	}
	

	public void altaDocente(String nickName, String nombre, String apellido, String email, Date fechaNacimiento, Boolean esDocente, String nombreInstituto) {
		Docente d = new Docente();
		
		d.setNickName(nickName);
		d.setNombre(nombre);
		d.setApellido(apellido);
		d.setCorreo(email);
		d.setFechaNacimiento(fechaNacimiento);
		d.setEsDocente(esDocente);
		d.setNombreInsituto(nombreInstituto);
		
		usuarios.add(d);
		
	}
	
	public Docente getUnDocente(String docente) {
		
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Docente d = em.find(Docente.class, docente);			
		return d;
	}


	//obtiene lista de nombre de usuarios
	public ArrayList<String> obtenerUsuarios(){
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
			
		Query query = em.createQuery("select u from Usuario u");
		List<Usuario> listUsuario = (List<Usuario>) query.getResultList();
				
		//Inicializo un ArrayList de string.
		ArrayList<String> aRetornar = new ArrayList<>();
		//Recorro la coleccion de Usuarios.
		for(Usuario u: listUsuario) {
			//Se llena la ArrayList con los nombres de los Usuarios.
			aRetornar.add(u.getNickName());
		}
		//Se retorna la lista.
		return aRetornar;
	}
			
	public Usuario obtenerUsuario(String nick) {
		Usuario usuario = null;
			
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Query query = em.createQuery("select u from Usuario u");
		List<Usuario> listUsuario = (List<Usuario>) query.getResultList();
			
		for(Usuario u: listUsuario) {
			if(u.getNickName().equals(nick)) {
				usuario = u;
			}
		}
		return usuario;
	}
	
	public List<String> obtenerEdicionesDeEstudiante(String nickName){
		List<String> ediciones = new ArrayList<String>();
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Query query = em.createQuery("select e from InscripcionEdc e");
		List<InscripcionEdc> listInscripEdc = (List<InscripcionEdc>) query.getResultList();
		for(InscripcionEdc iedc: listInscripEdc) {
			if(iedc.getUsuario().getNickName().equals(nickName)) {
				ediciones.add(iedc.getEdicion().getNombre());
			}
		}
		return ediciones;
	}
	
}
	

