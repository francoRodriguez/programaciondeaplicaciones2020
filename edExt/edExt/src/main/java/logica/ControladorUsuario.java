package logica;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import datatypes.DtDocente;
import datatypes.DtEdicion;
import excepciones.EmailExistenteException;
import excepciones.NickNameExistenteException;
import interfaces.IControladorUsuario;
import persistencia.Conexion;

@SuppressWarnings("unused")
public class ControladorUsuario implements IControladorUsuario {

	private static EntityManagerFactory emf;
	private static EntityManager em;
	
	public ControladorUsuario() {
		super();
	}


	//@SuppressWarnings("unused")
	@Override
	public void altaUsuarioEstudiante(String nickName, String nombre, String apellido, String correo, Date fechaNacimiento, Boolean esDocente) throws NickNameExistenteException, EmailExistenteException {
	
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario nick = mU.existeUsuario(nickName);
		String email = mU.existeEmail(correo);
		if(nick != null)
			 throw new NickNameExistenteException("El nickname " + nickName + " ya esta registrado");
		if(email != null)
			throw new EmailExistenteException("El email " + correo + " ya esta registrado");
		
		Usuario us;
		if(nick == null && email == null) {
				us = new Estudiante(nickName, nombre, apellido, correo, fechaNacimiento, esDocente);
				mU.add(us);
			}
	}
	
	
	public void altaUsuarioDocente(String nickName, String nombre, String apellido, String correo, Date fechaNacimiento,	Boolean esDocente, String nombreInstituto) throws NickNameExistenteException, EmailExistenteException {
		
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario nick = mU.existeUsuario(nickName);
		String email = mU.existeEmail(correo);
		if(nick != null)
			 throw new NickNameExistenteException("El nickname " + nickName + " ya esta registrado");
		if(email != null)
			throw new EmailExistenteException("El email " + correo + "ya esta registrado");
		
		Usuario us;
		if(nick == null && email == null) {
			us = new Docente(nickName, nombre, apellido, correo, fechaNacimiento, esDocente, nombreInstituto);
				mU.add(us);		
		}
	}
	
	// Devuelve solo los NICK de los docentes
	public List<String> listarDocentes() {
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();		
		return mU.getDocentes();
	}
	
	public List<String> listarEstudiantes() {
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();		
		return mU.getEstudiantes();
	}	
	
	// devuelve el docente cuyo NICK es pasado por parametro 
	public Docente seleccionarDocente(String docente) {
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		return mU.getUnDocente(docente);
	}
	
	public Usuario seleccionarUsuario(String nick) {
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario u = null;
		u = mU.obtenerUsuario(nick);
		return u;
	}

	
	@Override
	public String[] listarUsuarios() {
		//Inicializo un ArrayList de tipo String.
		ArrayList<String> usuarios;
		//Obtengo una instancia de Manejador.
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		//Obtengo la lista de Usuarios.
		usuarios = mU.obtenerUsuarios();
		
		String[] usuarios_ret = new String[usuarios.size()];
		int i = 0;
		for(String u:usuarios) {
			usuarios_ret[i] = u;
			i++;
		}
		return usuarios_ret;
	}
	
	
	//obtiene lista de nicknames de usuarios
	public ArrayList<String> obtenerUsuarios(){							
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
			
		Query query = em.createQuery("select u from Usuarios u");
		List<Usuario> listUsuario = (List<Usuario>)	query.getResultList();
		
		ArrayList<String> aRetornar = new ArrayList();
		for(Usuario u: listUsuario) {
			aRetornar.add(u.getNickName());
		}
		return aRetornar;
	}
	
	
	public Instituto seleccionarUnInstituto(String nomInstituto) {
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();
		Instituto ins = mi.buscarInstituto(nomInstituto);
		return ins;
	}
	
	public List<Edicion> buscarEdiciones(Instituto i){
		List<Edicion> edicionFinal = null;
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		
		
		List<Curso> cursos = i.getCursos();
		
		for(Curso c: cursos) {
			List<Edicion> ediciones = c.getEdiciones();
			for(Edicion e: ediciones) {
				edicionFinal.add(e);
			}
		}
		return edicionFinal;
	}
	
	public void modificarDatos(String nickName, String nombre, String apellido, String Correo, Date fechaNac) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		 
		Usuario usu= (Usuario)em.find(Usuario.class ,nickName);
		
		em.getTransaction().begin();

		usu.setNombre(nombre);
		usu.setApellido(apellido);
		usu.setFechaNacimiento(fechaNac);
		
		em.getTransaction().commit();
	}
	
	public String[] seleccionarInscripcionED(String nickName){
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		List<String> ediciones;
		ediciones = mU.obtenerEdicionesDeEstudiante(nickName);
		int i = 0;
		String[] miarray = new String[ediciones.size()];
		if(ediciones != null) {
			for(String e: ediciones) {
				miarray[i] = e;
				i++;
			}
		}
		return miarray;
	}
}

