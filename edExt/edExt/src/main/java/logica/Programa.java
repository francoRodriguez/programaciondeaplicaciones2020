package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import datatypes.DtCurso;
import datatypes.DtPrograma;
@Entity
public class Programa {
	@Id
	private String nombre;
	private String descripcion;
	private Date fechaI;
	private Date fechaF;
	@OneToMany(cascade=CascadeType.ALL)//,orphanRemoval=true)
	private List<Curso> cursos= new ArrayList<Curso>();
	
	@OneToMany(mappedBy = "programa",cascade = CascadeType.ALL, orphanRemoval=true)
	private List<InscripcionPF> inscripcionesPF = new ArrayList<>();

	public Programa(String nombre, String descripcion, Date fechaI, Date fechaF) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fechaI = fechaI;
		this.fechaF = fechaF;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Date getFechaI() {
		return fechaI;
	}
	public void setFechaI(Date fechaI) {
		this.fechaI = fechaI;
	}
	public Date getFechaF() {
		return fechaF;
	}
	public void setFechaF(Date fechaF) {
		this.fechaF = fechaF;
	}
	public List<Curso> getCursos() {
		return cursos;
	}
	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}
	public boolean existeCurso(String NombreCurso) {
		boolean retorno = false;
		for(Curso cur: cursos) {
			if(cur.getNombre().equals(NombreCurso)) retorno=true;
		}
		return retorno;
	}

	DtPrograma getDtPrograma() {
	DtPrograma dtc;
	dtc= new DtPrograma(this.nombre, this.descripcion, this.fechaI, this.getFechaF());
		return 	dtc;
	}
	public Programa() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public DtCurso getDtCurso(String nombreCurso) {
		DtCurso curso = new DtCurso();
		for(Curso aux: this.getCursos()) {
			if(aux.getNombre().compareTo(nombreCurso) == 0) {
				curso = aux.getDtCurso();
			}
		}
		return curso;
	}
	
	List<String> cursosDePrograma(){
		List<String> scursos =new ArrayList<String>();
		for(Curso c:cursos) {
			scursos.add(c.getNombre());
		}
		return scursos;
	}
}
