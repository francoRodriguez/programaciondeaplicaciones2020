package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import persistencia.Conexion;

public class ManejadorPrograma {
	private static ManejadorPrograma instancia = null;
	
	//private List<Programa> programas = new ArrayList<Programa>();
	
	private ManejadorPrograma(){
		///////////PARA//PRUEBAS//////////

		//Programa p1= new Programa("Programa1" ,  "Un programa", new Date(), new Date());
		//Programa p2= new Programa("Programa2" ,  "Un programa", new Date(),new Date());
		//this.addPrograma(p1);
		/////////BORRAR////////////////

	}
	
	public static ManejadorPrograma getInstancia() {
		if (instancia == null)
			instancia = new ManejadorPrograma();
		return instancia;
	}
	// Es un copy paste del manejador de institutos
	public List<String> listarProgramars() {	
		/*
		List<String> salida = new ArrayList<String>();
		for(Programa p: programas) {
			salida.add(p.getNombre());
		} 
		return salida;
		*/
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		
		Query query = em.createQuery("select p from Programa p");
		List<Programa> listPrograma = (List<Programa>) query.getResultList();
		
		ArrayList<String> aRetornar = new ArrayList<>();
		for(Programa p: listPrograma) {
			aRetornar.add(p.getNombre());
		}
		return aRetornar;
	}

	public List<Programa> getProgramas() {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		
		Query query = em.createQuery("select p from Programa p");
		List<Programa> listPrograma = (List<Programa>) query.getResultList();
		return listPrograma;
	}
	
	public void addPrograma(Programa p) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		em.getTransaction().begin();
		
		em.persist(p);
		
		em.getTransaction().commit();
	}
	
	public Programa find(String nombre) {
		Programa programa = null;
		
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Query query = em.createQuery("select p from Programa p");
		List<Programa> listPrograma = (List<Programa>) query.getResultList();
		
		for(Programa p: listPrograma) {
			if(p.getNombre().equals(nombre)) {
				programa = p;
			}
		}
		return programa;
		/*
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		
		Programa programa = em.find(Programa.class, nombre);
		return programa;*/
	}


}
