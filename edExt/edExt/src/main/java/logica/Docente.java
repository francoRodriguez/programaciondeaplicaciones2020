package logica;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

import datatypes.DtDocente;

@Entity
@DiscriminatorValue("Docente")
public class Docente extends Usuario{

	@Column(name = "nombreInstituto")
	String nombreInsituto;
	//@Column(name = "inst_DondeDictaCurso")
	//private List<Instituto> insitutosDelDocente= new ArrayList<Instituto>();
	
	@ManyToMany(cascade= CascadeType.ALL, mappedBy = "listaDocente")
	private List<Edicion> edicionesInscripto = new ArrayList<Edicion>();
	
	
	public String getNombreInsituto() {
		return nombreInsituto;
	}

	public void setNombreInsituto(String nombreInsituto) {
		this.nombreInsituto = nombreInsituto;
	}

	public List<Edicion> getEdicionesInscripto() {
		return edicionesInscripto;
	}

	public void setEdicionesInscripto(List<Edicion> edicionesInscripto) {
		this.edicionesInscripto = edicionesInscripto;
	}

	public Docente() {}
	
	public Docente(String nickName, String nombre, String apellido, String correo, Date fechaNacimiento, Boolean esDocente, String nombreInstituto) {
		super (nickName, nombre, apellido, correo, fechaNacimiento, esDocente);
		this.nombreInsituto= nombreInstituto ;
		}
	
}
