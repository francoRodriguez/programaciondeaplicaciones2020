package logica;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import persistencia.InscripcionPFID;
@Entity
@IdClass(InscripcionPFID.class)
public class InscripcionPF {
	@Temporal(TemporalType.DATE)
	private Date fechaIns ;
	@Id
	@ManyToOne
	private Programa programa;
	@Id
	@ManyToOne
	private Estudiante inscripto;
	
	
	public Estudiante getInscripto() {
		return inscripto;
	}
	public void setInscripto(Estudiante inscripto) {
		this.inscripto = inscripto;
	}
	public Date getFechaIns() {
		return fechaIns;
	}
	public void setFechaIns(Date fechaIns) {
		this.fechaIns = fechaIns;
	}
	public Programa getPrograma() {
		return programa;
	}
	public void setPrograma(Programa programa) {
		this.programa = programa;
	}
}

