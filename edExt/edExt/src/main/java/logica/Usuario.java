package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import datatypes.DtDocente;

import org.hibernate.annotations.Type;

@SuppressWarnings("unused")
@Entity
@Table(name="USUARIOS")

@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
public abstract class Usuario {

	@Id
	@Column(name = "nickName")
	private String nickName;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "apellido")
	private String apellido;
	@Column(name = "correo")
	private String correo;
	@Column(name = "fNacimiento")
	@Basic
	@Temporal(TemporalType.DATE)
	private Date fechaNacimiento;
	@Column(name = "esDocente")
	@Type(type  ="yes_no")
	private Boolean esDocente;
	
	

	/*@JoinTable(	name = "usuario_edicion",
				joinColumns 		= {@JoinColumn(	name = "nickName")},
				inverseJoinColumns 	= {@JoinColumn(	name = "nombre_edicion")})*/
	
	
	@OneToMany (mappedBy= "usuario",cascade= CascadeType.ALL,orphanRemoval=true)
	private List<InscripcionEdc> inscripciones = new ArrayList<>();
	

	public Usuario() {
		super();
	}
	
	public Usuario(String nickName, String nombre, String apellido, String correo, Date fechaNacimiento2, Boolean esDocente) {
		super();
		this.setNickName(nickName);
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setCorreo(correo);
		this.setFechaNacimiento(fechaNacimiento2);
		this.setEsDocente(esDocente);
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	public void setEsDocente(Boolean esDocente) {
		this.esDocente = esDocente;
	}
	
	public Boolean getEsDocente() {
		return esDocente;
	}
	///get y set de lista Inscripciones///
	public List<InscripcionEdc> getInscripciones() {
		return inscripciones;
	}
	public void setInscripciones(List<InscripcionEdc> inscripciones) {
		this.inscripciones = inscripciones;
	}
	/*
	DtDocente getDtDocente() {
		return new DtDocente(this.nickName, this.nombre, this.apellido, this.correo, this.fechaNacimiento);
		
	}
	*/

	
	
}
