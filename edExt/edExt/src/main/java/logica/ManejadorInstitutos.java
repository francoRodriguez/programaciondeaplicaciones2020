package logica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import datatypes.DtCurso;
import datatypes.DtEdicion;
import persistencia.Conexion;

public class ManejadorInstitutos {
	private static ManejadorInstitutos instancia 	= null;	
	//private List<Instituto> institutos 				= new ArrayList<Instituto>();
	
	public ManejadorInstitutos(){}
	
	public static ManejadorInstitutos getInstancia() {
		if (instancia == null)
			instancia = new ManejadorInstitutos();
		return instancia;
	}
	
/*	public void agregarInstituto(String nombre) {
		Instituto i = new Instituto();
		i.setNombre(nombre);		
		institutos.add(i);

	}
*/	
	public List<Instituto> getInstitutos() {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();		
		Query query = em.createQuery("select i from Instituto i");
		List<Instituto> listInstituto = (List<Instituto>) query.getResultList();
		return listInstituto;
	}

	public void setInstitutos(List<Instituto> institutos) {
		//this.institutos = institutos;
	}

	public void agregarCurso(String nombre, String descripcion, String duracion, int cantidadHoras, int cantidadCreditos, String url,Date fechaAlta,
			List<Edicion> ediciones, Instituto instituto) {
		
		Curso c = new Curso();
		c.setNombre(nombre);
		c.setDescripcion(descripcion);
		c.setDuracion(duracion);
		c.setCantidadHoras(cantidadHoras);
		c.setCantidadCreditos(cantidadCreditos);	
		c.setUrl(url);
		c.setFechaAlta(fechaAlta);
		c.setEdiciones(ediciones);
		
		// a este instituto que le paso por parametro le agrego el curso.
		instituto.add(c);					
		
		System.out.println("Instituto: " + instituto);
		System.out.println("Lista Cursos");		
		List<Curso> cursos = instituto.getCursos();
		for(Curso c1: cursos) {
			System.out.println(c1.getNombre());
		} 
				
	}
	
	
	public Instituto buscarInstituto(String nombre) {
		/*
		Instituto instituto=null;
		for(Instituto i: institutos) {
			if (i.getNombre().equals(nombre))
				instituto=i;
		}
		return instituto;
		*/
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		
		Instituto instituto = em.find(Instituto.class, nombre);
		return instituto;
	}
	
	// devuleve todos los nombres de los institutos cargados.		
	public List<String> listarInstituto() {	
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();		
		Query query = em.createQuery("select i from Instituto i");
		List<Instituto> listInstituto = (List<Instituto>) query.getResultList();
		List<String> salida = new ArrayList<String>();
		for(Instituto i: listInstituto) {
			salida.add(i.getNombre());
		} 
		return salida;
	}
	
	public boolean exists (String nombre) {
		boolean retorno=false;
		if(this.getInstituto(nombre)==null) {
			retorno=false;
		}else {
			retorno=true;			
		}
		/*for(Instituto i: institutos) {
			if(i.getNombre().equals(nombre)){
				retorno= true;
			}}
		}*/
		
		return retorno;
	}
	public void add(Instituto i) {
		//institutos.add(i);
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		em.getTransaction().begin();
		
		em.persist(i);
		//System.out.print("ACAAAAAAAAAAAAAAAAAA");
		  
		em.getTransaction().commit();
	}
	
	public Instituto getInstituto(String nombreIns) {
		//Instituto in=null;
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		
		Instituto in = em.find(Instituto.class, nombreIns);
		
		/*for(Instituto i: institutos) {
			if(i.getNombre().equals(nombreIns)){
				return in = i;
			}
		}*/
		
		return in;
	}
			
	public DtCurso getNombreCurso(Instituto i, String nombreCurso) {
		List<Curso> cursos = i.getCursos();		
		for(Curso c: cursos) {
			if(c.getNombre().equals(nombreCurso)) {
				return c.getDtCurso();
			}			
		}
		return null;
	}
	
	
	
	public boolean altaEdicionCurso(Instituto i, String nombreCurso, String nombreEdicion, Date fechaI, Date fechaF, int cupo, Date fechaPub,
			List<Docente> listaDocentes) {
		System.out.println("Manejador listaDocentes:" + listaDocentes.size());
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		
			
		// Busco el curso pasado por parametro para la instancia instituto i
		//Curso c = i.obtenerCurso(nombreCurso);
		Curso c = em.find(Curso.class, nombreCurso);
		

		// si la edicion ya existe devuelvo false y me voy.
		
		//if (c.existeEdicion(nombreEdicion)) {
		if (em.find(Edicion.class, nombreEdicion) != null) {
			return false;
		} else {
			// Doy de alta la edicion:
			Edicion e = new Edicion(nombreEdicion, fechaI, fechaF, cupo, fechaPub, listaDocentes);
			for(Docente d : listaDocentes) {
				d.getEdicionesInscripto().add(e);
			}
			c.addEdicion(e);
			
			List<Docente> doc = e.getListaDocente();
			System.out.println("Manejador listaDocentes en memoria:" + doc.size());
			
			em.getTransaction().begin();
			//em.persist(e);
			em.persist(c);
			em.getTransaction().commit();
			
			// salgo con true
			return true;
				
		}
	}
	

}
