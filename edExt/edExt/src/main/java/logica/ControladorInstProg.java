package logica;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import datatypes.DtCurso;
import datatypes.DtEdicion;
import datatypes.DtPrograma;
import excepciones.EmailExistenteException;
import excepciones.NickNameExistenteException;
import excepciones.ProgramaExisteException;
import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;
import persistencia.Conexion;
public class ControladorInstProg implements IControladorInstProg {
	private  Instituto i; // es una variable para "recordar" el instituto, ya s� es horrible pero no encontre como se hace
	//sacarle el public lo hice para testear
	private Programa p;
	public ControladorInstProg() {
		super();
		// TODO Auto-generated constructor stub
	}
	 
	
	public String[] listarInstitutosArray() { // para el caso de uso Alta Curso
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();	
		System.out.println("Listando..Nada" );
		List<String> salida = mi.listarInstituto();
	       if (salida.isEmpty())
	            return null;
	        else {
	            Object[] o = salida.toArray();
	            String[] miarray = new String[o.length];
	            for (int i = 0; i < o.length; i++) {
	            	miarray[i] = (String) o[i];
	            }	            
	            return miarray;
	        }
	}
	@Override
	public String[] listarInstitutos() {		
		ManejadorInstitutos mI = ManejadorInstitutos.getInstancia();
		List<String> Institutos =  mI.listarInstituto();
		for(String s: Institutos) System.out.println("Listando.." + s);
		String[] Institutos_ret = new String[Institutos.size()];
        int i=0;
        for(String s:Institutos) {
        	Institutos_ret[i]=s;
        	i++;
        }
        return Institutos_ret;
	}
	
	public boolean ingresarNomInst(String nom) {
		boolean existe= true;
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();
		if (!mi.exists(nom)) {
			i = new Instituto();
			i.setNombre(nom);
			mi.add(i);
			//System.out.println("llego " + i.getNombre());
			existe = false;
		}else {
			System.out.println("Ac� seria la excepcion ese instituto ya existe ");
			existe = true;
		}
		return existe;
	}

	public void CancelarAltaInstituto() {
		i=null;
	}
	public boolean nuevoCurso(String nombreCurso ) {
		//i=null;
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();
		List<Instituto> institutos = mi.getInstitutos();
		boolean noexiste = true;
		for(Instituto ins :institutos) {
			if(!ins.noExisteCurso(nombreCurso)) {
				noexiste=false;
			}
		}
		if(noexiste) {
			System.out.println("Se Crea el curso Static ");
			i.crearCursoStatic(nombreCurso);
		}
		return noexiste;
	}
	public void seleccionarInstituto(String nombreInst) {
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();	
		i = mi.getInstituto(nombreInst);
	}
		
	public void setearCurso(String nombreCurso, String desc, String duracion, int cantHoras, int cantCreditos, String url, Date fechaAlta) {
		i.setearCurso(nombreCurso, desc, duracion, cantHoras, cantCreditos, url, fechaAlta);
	}
	public String[] listarPrevias(){
		List<String> cursos=i.listarNombreCursos();
		//for(String s:cursos ) System.out.println(s + " Institutos.listarPrevvias.List");
		String[] miarray = new String[cursos.size()];
		miarray=cursos.toArray(miarray);
		//for(String s:miarray ) System.out.println(s + " Institutos.listarPrevvias.Array");
		return miarray;
	}
	public void seleccionarPrevias(List<String> previas) {
		i.setearPrevias(previas);
	}
	public void confirmarAltaCurso() {
		i.confirmarAltaCurso();
		//i=null;
	}
	public String[] listarNombreCursos(String nombreInst){ //previamente nombreCursos de mostrar cursos
		List<String> cursos;
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();	
		i = mi.getInstituto(nombreInst);
		cursos = i.listarNombreCursos();
		String[] miarray = new String[cursos.size()];
		miarray=cursos.toArray(miarray);	
		return miarray;
	}
	public DtCurso mostrarDatosdeCurso(String nombreCurso) {
		return i.mostrarDatosdeCurso(nombreCurso);
	}
	
	public DtCurso mostrarDatosdeCurso(String nombreCurso, Instituto i) {
		return i.mostrarDatosdeCurso(nombreCurso);
	}
	public boolean AltaEdicionCurso(String nombreInstituto, String nombreCurso, String nombreEdicion, Date fechaI, Date fechaF,
			int cupo, Date fechaPub, List<Docente> listaDocentes) {
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();	
		i = mi.getInstituto(nombreInstituto);	
		
		System.out.println("Controlador listaDocentes:" + listaDocentes.size());
		return mi.altaEdicionCurso(i, nombreCurso, nombreEdicion, fechaI, fechaF, cupo, fechaPub, listaDocentes);
		
	} 
	
	public String[] listarEdicionesdeCurso(String nombreCurso){
		List<String> ediciones;
		ediciones=i.obtenerEdiciones(nombreCurso);
		String[] miarray = new String[ediciones.size()];
		miarray=ediciones.toArray(miarray);	
		return miarray;
	}
	public String[] listarProgramasCurso(String nombreCurso){
		ManejadorPrograma mp = ManejadorPrograma.getInstancia();
		List<String> listast= new ArrayList<String>();
		System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACCCCCCCCCCCCAAAAAAAAAAA");
		List<Programa> listap = mp.getProgramas();
		for(Programa p:listap) {
			if(p.existeCurso(nombreCurso) || nombreCurso=="*") {
				listast.add(p.getNombre());
			}
		}
		String[] miarray = new String[listast.size()];
		miarray=listast.toArray(miarray);
		return miarray;
	}
	public Programa mostrarPrograma(String nombrePrograma) {
		ManejadorPrograma mp = ManejadorPrograma.getInstancia();
		p=mp.find(nombrePrograma);
		return p;
	}
	
	public String[] cursosDePrograma(){	//funciona con mostrarPrograma que recuerda p
		List<String> programas=p.cursosDePrograma();
		String[] miarray = new String[programas.size()];
		miarray=programas.toArray(miarray);
		return miarray;
	}
	public DtEdicion mostrarEdicion(String nombreEdicion) {
		return i.obtenerEdicion(nombreEdicion);
	}
	public void cancelarConsultaCurso() {
		i.borrarC();
		i=null;
		p=null;
	}
	
	public DtEdicion DatosEdicionCurso(Edicion e) {		
		DtEdicion datosEdicion = new DtEdicion();
		datosEdicion.setNombre(e.getNombre());
		datosEdicion.setFechaI(e.getFechaI());
		datosEdicion.setFechaF(e.getFechaF());
		datosEdicion.setFechaPub(e.getFechaPub());
		//datosEdicion.setInscriptos(e.getInscriptos()); // lo comente (Wueis) pq me daba error, descomentar
		datosEdicion.setCupo(e.getCupo());
		return datosEdicion;
	}
	public void cancelarAltaCurso() {
		if(i!=null) {
			i.c=null;
			i=null;
		}
	}
	
	public void cancelarAltaEdicionCurso() {
		if(i!=null) {
			i.c=null;
			i=null;
		}
	}

	///////////////////PROGRAMA//////////
	public void altaPrograma(String nombrePrograma, String descripcion, Date fechaI, Date fechaF) throws ProgramaExisteException {

		ManejadorPrograma mp = ManejadorPrograma.getInstancia();
		Programa programa = mp.find(nombrePrograma);
		if(programa != null)
			 throw new ProgramaExisteException("El programa " + nombrePrograma + " ya esta registrado");
		
		Programa pr;
		if(programa == null ) {
				pr = new Programa(nombrePrograma, descripcion, fechaI, fechaF);
				mp.addPrograma(pr);
			}
	}
	//////////////PROGRAMA////////////////
	public Instituto obtenerInstituto(String nomins){
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();
		Instituto retorno=null;
		for (Instituto iterador:mi.getInstitutos()) {
			if(iterador.getNombre()==nomins) {
				retorno = iterador;
			}
		}
		return retorno;
	}
	public Curso obtenerCurso(Instituto ins, String nomcur) {
		Curso retorno = null;
		for (Curso Iterador: ins.getCursos()) {
			if(nomcur==Iterador.getNombre()) {
				retorno = Iterador;
			}
		}
		return retorno;
	}
	public Edicion obtenerEdicion(Curso cur, String nomedi) {
		Edicion retorno = null;
		for (Edicion Iterador: cur.getEdiciones()) {
			if(nomedi==Iterador.getNombre()) {
				retorno = Iterador;
			}
		}
		return retorno;
	}
	public Edicion obtenerEdicionVigente(Curso objetivo) {
		Edicion retorno = null;
		Date fechaactual = new Date();
		for(Edicion iterador:objetivo.getEdiciones()) {
			if (iterador.getFechaI().compareTo(fechaactual)>0) {
				retorno=iterador;
			}
		}
		return retorno;
	}


	@Override
	public void realizarInscripcion(String instituto, String curso, String edicion, String estudiante, IControladorUsuario iconus){
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		
		Instituto objetoInstituto = obtenerInstituto(instituto);
		Curso objetoCurso = obtenerCurso(objetoInstituto,curso);
		Edicion objetoEdicion = obtenerEdicion(objetoCurso,edicion);
		Usuario objetoUsuario = mU.existeUsuario(estudiante);
		
		objetoEdicion.agregarInscripcion(objetoUsuario);
	}
	
	public Edicion seleccionarEdicion(String nombreE) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Edicion edicion = em.find(Edicion.class, nombreE);
		
		return edicion;
	}
	
	public String[] listarProgramas() {
		List<String> programas;
		ManejadorPrograma mP = ManejadorPrograma.getInstancia();
		programas = mP.listarProgramars();
		String[] programas_ret = new String[programas.size()];
		int i = 0;
		for(String p:programas) {
			programas_ret[i] = p;
			i++;
		}
		return programas_ret;
	}
}
