package logica;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import persistencia.InscripcionEdcID;
@Entity
@IdClass(InscripcionEdcID.class)
public class InscripcionEdc {
	@Id 
	@ManyToOne
	private Edicion edicion;
	
	@Id
	@ManyToOne
	private Usuario usuario;
	
	@Temporal(TemporalType.DATE)
	private Date fecha;
	
	
	public Edicion getEdicion() {
		return edicion;
	}
	public void setEdicion(Edicion edicion) {
		this.edicion = edicion;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	public InscripcionEdc() {
		super();
	}
	public InscripcionEdc(Date fecha, Usuario usuario, Edicion edicion) {
		super();
		this.fecha = fecha;
		this.usuario = usuario;
		this.edicion = edicion;
	}
}
