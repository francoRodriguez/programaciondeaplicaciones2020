package interfaces;

import java.util.Date;
import java.util.List;

import datatypes.DtCurso;
import datatypes.DtEdicion;
import datatypes.DtPrograma;
import excepciones.ProgramaExisteException;
import logica.Curso;
import logica.Docente;
import logica.Edicion;
import logica.Instituto;
import logica.Programa;
import logica.Usuario;


@SuppressWarnings("unused")
public interface IControladorInstProg {
	//Alta Instituto
	public boolean ingresarNomInst(String nom);
	public void CancelarAltaInstituto();
	//Alta Curso
	public String[] listarInstitutosArray();
	public void seleccionarInstituto(String nombreInst);
	public boolean nuevoCurso(String nombreCurso );
	public void setearCurso(String nombreCurso, String desc, String duracion, int cantHoras, int cantCreditos, String url, Date fechaAlta);
	public String[] listarPrevias();
	public void seleccionarPrevias(List<String> previas);
	public void confirmarAltaCurso();
	public void cancelarAltaCurso();
	// Consulta Curso 
	//Reutilizo>>listarInstitutosArray() >> eleccionarInstituto(String nombreInst)
	String[]  listarNombreCursos(String nombreInst);
	public DtCurso mostrarDatosdeCurso(String nombreCurso);
	public String[] listarEdicionesdeCurso(String nombreCurso);
	String[] listarProgramasCurso(String nombreCurso);
	public Programa mostrarPrograma(String nombrePrograma);
	public String[] cursosDePrograma(); // esta la agregu� pq el dt no los trae falta el DC
	public DtEdicion mostrarEdicion(String nombreEdicion);
	public void cancelarConsultaCurso();

	//Franco 
	// Alta Edicion Curso
	//
	//public String[] listarInstitutosArray();
	//public void seleccionarInstituto(String nombreInst);
	//List<String> listarNombreCursos(String nombreInst);
	public boolean AltaEdicionCurso(String nombreInst, String nombreCurso, String nombreEdicion, Date fechaI,
			Date fechaF, int cupo, Date fechaPub, List<Docente> listaDocentes);
	public void cancelarAltaEdicionCurso();
	public String[] listarInstitutos();
	
	
	// DatosEdicionCurso
	//public String[] listarInstitutosArray();
	//public void seleccionarInstituto(String nombreInst);
	//List<String> listarNombreCursos(String nombreInst);
	//public List<String> listarEdicionesdeCurso(String nombreCurso);
	//public DtEdicion mostrarEdicion(String nombreEdicion);
	
	//AltaPrograma
	public void altaPrograma(String nombrePrograma, String descripcion, Date fechaI, Date fechaF) throws ProgramaExisteException;
	public Instituto obtenerInstituto(String nomins);
	public Curso obtenerCurso(Instituto ins, String nomcur);
	public Edicion obtenerEdicionVigente(Curso objetivo);
	public void realizarInscripcion(String instituto, String curso, String edicion, String estudiante,IControladorUsuario iconus);


	//ConsultaUsuario

	public Edicion seleccionarEdicion(String nombreE);
	
	//Consulta Programa
	public String[] listarProgramas();
}
