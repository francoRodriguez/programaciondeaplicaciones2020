package interfaces;

import java.util.Date;
import java.util.List;

import datatypes.DtDocente;
import datatypes.DtEdicion;
import excepciones.EmailExistenteException;
import excepciones.NickNameExistenteException;
import logica.Docente;
import logica.Edicion;
import logica.Instituto;
import logica.Usuario;

@SuppressWarnings("unused")
public interface IControladorUsuario{
	public void altaUsuarioDocente(String nickName, String nombre, String apellido, String correo, Date fechaNacimiento, Boolean esDocente, String nombreInstituto) throws NickNameExistenteException, EmailExistenteException;
	public void altaUsuarioEstudiante(String nickName, String nombre, String apellido, String correo, Date fechaNacimiento, Boolean esDocente) throws NickNameExistenteException, EmailExistenteException;
	public List<String>	listarEstudiantes();
	public List<String> listarDocentes();
	//	public Usuario existeUsuario(String nickName);
	//	public String existeEmail(String email);
	public Docente seleccionarDocente(String docente);
	public String[] listarUsuarios();
	public Usuario seleccionarUsuario(String nick);
	
	public Instituto seleccionarUnInstituto(String nomInstituto);
	public List<Edicion> buscarEdiciones(Instituto i);
	public void modificarDatos(String nickName, String nombre, String apellido, String Correo, Date fechaNac);
	
	public String[] seleccionarInscripcionED(String NickName);
	}


 
