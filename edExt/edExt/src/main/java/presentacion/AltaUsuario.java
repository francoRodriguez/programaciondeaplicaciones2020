package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import excepciones.EmailExistenteException;
import excepciones.NickNameExistenteException;

import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

import interfaces.IControladorUsuario;
import logica.Instituto;
import interfaces.IControladorInstProg;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.event.ChangeListener;

import com.toedter.calendar.JDateChooser;

import javax.swing.event.ChangeEvent;

@SuppressWarnings("unused")
public class AltaUsuario extends JInternalFrame {
	
private static final long serialVersionUID = 1L;
	
	private IControladorUsuario iconUs;
	private IControladorInstProg iconIP;
	
	public boolean esDocente=false;
	
	
	private JTextField textFieldNickname;
	private JTextField textFieldEmail;
	private JTextField textFieldNombre;
	private JTextField textFieldApellido;
	private JComboBox<String> comboBoxListarInstitutos;
	private JCheckBox chckbxEsDocente;
	private JDateChooser dateChooserFN;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 * @param iconIP 
	 */
	public AltaUsuario(IControladorUsuario iconUs, IControladorInstProg iconIP) {
		
		this.iconUs = iconUs;
		this.iconIP = iconIP;
		
		setTitle("edEXT");
		setClosable(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel LabelNickname = new JLabel("Nickname");
		LabelNickname.setBounds(30, 34, 61, 14);
		getContentPane().add(LabelNickname);
		
		JLabel LabelEmail = new JLabel("Email");
		LabelEmail.setBounds(215, 34, 46, 14);
		getContentPane().add(LabelEmail);
		
		textFieldNickname = new JTextField();
		textFieldNickname.setBounds(103, 31, 100, 20);
		getContentPane().add(textFieldNickname);
		textFieldNickname.setColumns(10);
		
		textFieldEmail = new JTextField();
		textFieldEmail.setColumns(10);
		textFieldEmail.setBounds(276, 31, 121, 20);
		getContentPane().add(textFieldEmail);
		
		JLabel LabelNombre = new JLabel("Nombre");
		LabelNombre.setBounds(30, 73, 46, 14);
		getContentPane().add(LabelNombre);
		
		JLabel LabelApellido = new JLabel("Apellido");
		LabelApellido.setBounds(215, 73, 46, 14);
		getContentPane().add(LabelApellido);
		
		textFieldNombre = new JTextField();
		textFieldNombre.setColumns(10);
		textFieldNombre.setBounds(102, 64, 101, 20);
		getContentPane().add(textFieldNombre);
		
		textFieldApellido = new JTextField();
		textFieldApellido.setColumns(10);
		textFieldApellido.setBounds(276, 70, 121, 20);
		getContentPane().add(textFieldApellido);
		
		
		JLabel SeleccioneInstitutoLabel = new JLabel("Seleccione Instituto");
		SeleccioneInstitutoLabel.setBounds(215, 132, 148, 14);
		getContentPane().add(SeleccioneInstitutoLabel);
		comboBoxListarInstitutos = new JComboBox<String>();
		comboBoxListarInstitutos.setToolTipText("Seleccione instituto");
		comboBoxListarInstitutos.setBounds(215, 159, 182, 20);
		getContentPane().add(comboBoxListarInstitutos);
		comboBoxListarInstitutos.setEnabled(false);
		
		
		JLabel lblFechaDeNacimiento = new JLabel("Fecha de nacimiento");
		lblFechaDeNacimiento.setBounds(30, 104, 177, 15);
		getContentPane().add(lblFechaDeNacimiento);
		dateChooserFN = new JDateChooser();
		dateChooserFN.setBounds(215, 100, 182, 19);
		getContentPane().add(dateChooserFN);
		
		
		JButton btnConfirmarAltaUsuario = new JButton("Confirmar");
		btnConfirmarAltaUsuario.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
			}
		});
		btnConfirmarAltaUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AltaUsuarioAceptarActionPerformed(e);
				}
		});
		btnConfirmarAltaUsuario.setBounds(304, 223, 89, 23);
		getContentPane().add(btnConfirmarAltaUsuario);
		
		JButton btnCancelarAltaUsuario = new JButton("Cancelar");
		btnCancelarAltaUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AltaUsuario.this.setVisible(false);
				limpiarFormulario();
			}
		});
		btnCancelarAltaUsuario.setBounds(30, 223, 89, 23);
		getContentPane().add(btnCancelarAltaUsuario);
		
		
		chckbxEsDocente = new JCheckBox("Es Docente");
		chckbxEsDocente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboBoxListarInstitutos.setEnabled(true);
			}
		});
		chckbxEsDocente.setBounds(30, 128, 121, 23);
		getContentPane().add(chckbxEsDocente);
		
	}
	
	
	@SuppressWarnings("unused")
	protected void AltaUsuarioAceptarActionPerformed(ActionEvent arg0) {
		String nickName = this.textFieldNickname.getText();
        String email = this.textFieldEmail.getText();
        String nombre = this.textFieldNombre.getText();
        String apellido = this.textFieldApellido.getText();
        int anioN = dateChooserFN.getCalendar().get(Calendar.YEAR);
		int mesN = dateChooserFN.getCalendar().get(Calendar.MONTH) + 1;
		int diaN = dateChooserFN.getCalendar().get(Calendar.DAY_OF_MONTH);
	    String fechaNString = diaN+"-"+mesN+"-"+anioN;
	    //String nombreInstituto = null;
        //Date fNac = new Date();
        DateFormat formatoFecha = new SimpleDateFormat("dd-MM-yyyy");
		Date fNac = null;
		try {
			fNac = formatoFecha.parse(fechaNString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		
        boolean esDocente= false;
        if(chckbxEsDocente.isSelected()) {
        	esDocente=true;
        }

        if(esDocente) {
        	String nombreInstituto = this.comboBoxListarInstitutos.getSelectedItem().toString();
            System.out.println(nombreInstituto);

        	if (checkFormulario()) {
                try {
                    iconUs.altaUsuarioDocente(nickName, nombre, apellido, email, fNac, esDocente, nombreInstituto); 
                    JOptionPane.showMessageDialog(this, "El usuario ha sido dado de alta", "Alta Ususario", JOptionPane.INFORMATION_MESSAGE);
                    limpiarFormulario();
                } catch (NickNameExistenteException | EmailExistenteException e) {
                    JOptionPane.showMessageDialog(this, e.getMessage(), "Alta de usuario", JOptionPane.ERROR_MESSAGE);
                	}
                setVisible(true);
            }
        	
        }
        else
        {
        	if (checkFormulario()) {
                try {
                    iconUs.altaUsuarioEstudiante(nickName, nombre, apellido, email, fNac, esDocente); 
                    JOptionPane.showMessageDialog(this, "El usuario ha sido dado de alta", "Alta Ususario", JOptionPane.INFORMATION_MESSAGE);
                    limpiarFormulario();
                } catch (NickNameExistenteException | EmailExistenteException e) {
                    JOptionPane.showMessageDialog(this, e.getMessage(), "Alta de usuario", JOptionPane.ERROR_MESSAGE);
                }
               
                setVisible(true);
            }
        }
	}
	
	
	
	private boolean checkFormulario() {
        String nickName = this.textFieldNickname.getText();
        String email = this.textFieldEmail.getText();
        if (nickName.isEmpty() || email.isEmpty()) {
        	if(nickName.isEmpty()) {
        		JOptionPane.showMessageDialog(this, "Debe introducir un nick name", "Alta Usuario", JOptionPane.ERROR_MESSAGE);
                return false;
        	}          
            else {
            	if(email.isEmpty()) {
            		JOptionPane.showMessageDialog(this, "Debe introducir un email", "Alta Usuario", JOptionPane.ERROR_MESSAGE);
                    return false;
            	}
            }
        }
        return true;
    }
	

	
	 private void limpiarFormulario() {
		 textFieldNickname.setText("");
		 textFieldNombre.setText("");
		 textFieldApellido.setText("");
		 textFieldEmail.setText("");
		 chckbxEsDocente.setSelected(false);
		 comboBoxListarInstitutos.setEnabled(false);
		 
	 }
	 
	 
	 public boolean iniciarlizarComboBoxes() {
		 if(iconIP.listarInstitutos().length == 0) {
			 JOptionPane.showMessageDialog(this, "No existen insitutos para dar de alta usuairos", "alta usuario", JOptionPane.WARNING_MESSAGE);
			 
			 return false;
		 }else {
			 
			 DefaultComboBoxModel<String> modelInstitutos = new DefaultComboBoxModel<String>(iconIP.listarInstitutosArray());
			 comboBoxListarInstitutos.setModel(modelInstitutos);
			 return true;
		 }

	 }
 }
	


