package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JTextField;

import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;

import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

@SuppressWarnings({ "unused", "serial", "rawtypes" })
public class AltaCurso extends JInternalFrame {
	private JTextField textFieldNombre;
	private JTextField textFielDesc;
	private JTextField textFielDuracion;
	private JTextField textFieldHoras;
	private JTextField textFieldCreditos;
	private JTextField textFieldUrl;
	private String[] listaCursos;
	private List<String> previas = new ArrayList<>();
	private boolean institutoSeleccionado = false;
	//private JComboBox combocursos;


	public AltaCurso(IControladorUsuario iconUs, final IControladorInstProg iconIP) {
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosed(InternalFrameEvent e) {
				textFieldNombre.setText("");
				textFielDesc.setText("");
				textFielDuracion.setText("");
				textFieldHoras.setText("");
				textFieldCreditos.setText("");
				textFieldUrl.setText("");	
			}
		});
		System.out.println("abreeeeeeeeeeeeEEEEEEEEEEEEEEE");

		//System.out.println("Se crea Alta Curso");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		this.setTitle("alta de Curso");
		this.setClosable(true);
		//DefaultListModel DLM= new DefaultListModel();

		JComboBox comboBoxListarInstitutos = new JComboBox();
		comboBoxListarInstitutos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//String s = comboBoxListarInstitutos.getSelectedItem().toString();
				//iconIP.seleccionarInstituto(s);
				//DLM.addElement("Curso");
			}
		});
		comboBoxListarInstitutos.setBounds(10, 33, 123, 21);
		getContentPane().add(comboBoxListarInstitutos);
		
		JLabel lblNewLabel = new JLabel("Previas");
		lblNewLabel.setBounds(288, 67, 55, 13);
		getContentPane().add(lblNewLabel);
		
		textFieldNombre = new JTextField();
		textFieldNombre.setBounds(20, 64, 96, 19);
		getContentPane().add(textFieldNombre);
		textFieldNombre.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre");
		lblNewLabel_1.setBounds(126, 67, 45, 13);
		getContentPane().add(lblNewLabel_1);
		
		textFielDesc = new JTextField();
		textFielDesc.setBounds(20, 93, 96, 19);
		getContentPane().add(textFielDesc);
		textFielDesc.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Descripcion");
		lblNewLabel_2.setBounds(126, 96, 58, 13);
		getContentPane().add(lblNewLabel_2);
		
		textFielDuracion = new JTextField();
		textFielDuracion.setBounds(20, 123, 96, 19);
		getContentPane().add(textFielDuracion);
		textFielDuracion.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Duracion");
		lblNewLabel_3.setBounds(126, 126, 45, 13);
		getContentPane().add(lblNewLabel_3);
		
		textFieldHoras = new JTextField();
		textFieldHoras.setBounds(20, 152, 96, 19);
		getContentPane().add(textFieldHoras);
		textFieldHoras.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Horas Int");
		lblNewLabel_4.setBounds(126, 155, 58, 13);
		getContentPane().add(lblNewLabel_4);
		
		textFieldCreditos = new JTextField();
		textFieldCreditos.setBounds(20, 181, 96, 19);
		getContentPane().add(textFieldCreditos);
		textFieldCreditos.setColumns(10);
		
		JLabel lblNewLabel_5 = new JLabel("Cr\u00E9ditos Int");
		lblNewLabel_5.setBounds(126, 184, 68, 13);
		getContentPane().add(lblNewLabel_5);
		
		textFieldUrl = new JTextField();
		textFieldUrl.setBounds(20, 210, 96, 19);
		getContentPane().add(textFieldUrl);
		textFieldUrl.setColumns(10);
		
		JLabel lblNewLabel_6 = new JLabel("URL");
		lblNewLabel_6.setBounds(126, 213, 45, 13);
		getContentPane().add(lblNewLabel_6);
		
		JComboBox combocursos; // lo pas� pq lo preciso ac�
		/*JComboBox*/ combocursos = new JComboBox();
		combocursos.setBounds(256, 92, 124, 21);
		getContentPane().add(combocursos);
		combocursos.setEnabled(false);
		//String[] lista;
		JButton btnCrear = new JButton("Crear");
		btnCrear.addActionListener(new ActionListener() {
			//@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent e) {
				if(!combocursos.isEnabled() && crear(iconIP, textFieldNombre )){
					combocursos.setEnabled(true);
					combocursos.removeAllItems();
					previas = new ArrayList<>();
					
					listaCursos = iconIP.listarPrevias();
					for(String s:listaCursos ) System.out.println(s + " SIACA");
					
					if(listaCursos != null && listaCursos.length>0) {
						String primerItem = listaCursos[0];
						for(String i : listaCursos){
							//System.out.println(i);
							combocursos.addItem(i);					
						}			
						combocursos.setSelectedItem(primerItem);
						getContentPane().add(combocursos);
						
					} else {
						combocursos.addItem("Sin Datos");	
						//comboBoxListarInstitutos.setEnabled(false);
					}
					
				}

			}
		});
		btnCrear.setBounds(31, 239, 85, 21);
		getContentPane().add(btnCrear);
		
		JButton btnSelInst = new JButton("Seleccionar Instituto");
		btnSelInst.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s = comboBoxListarInstitutos.getSelectedItem().toString();
				iconIP.seleccionarInstituto(s);
				comboBoxListarInstitutos.setEnabled(false);
				institutoSeleccionado=true;
			}
			
		});
		btnSelInst.setBounds(156, 33, 142, 21);
		getContentPane().add(btnSelInst);
		
		//aca iba combocursos
		
		JButton btnAniadir = new JButton("A\u00F1adir");
		btnAniadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s = combocursos.getSelectedItem().toString();
				System.out.println(s+"ACA");
				previas.add(s);
			}
		});
		btnAniadir.setBounds(281, 122, 85, 21);
		getContentPane().add(btnAniadir);
		
		JButton btnFin = new JButton("Confirmar");
		btnFin.setHorizontalAlignment(SwingConstants.LEFT);
		btnFin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for(String s: previas)System.out.println(s);
				iconIP.seleccionarPrevias(previas);
				iconIP.confirmarAltaCurso();
				JOptionPane.showMessageDialog(null, "Se ha creado el curso!","Alta curso", JOptionPane.OK_OPTION);
				iconIP.cancelarAltaCurso();  
				combocursos.removeAllItems();
				previas.clear();
				combocursos.setEnabled(false);
			}
		});
		btnFin.setBounds(321, 240, 96, 21);
		getContentPane().add(btnFin);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iconIP.cancelarAltaCurso();
				aVerSiMeDeja();
			}
		});
		btnCancelar.setBounds(197, 240, 85, 21);
		getContentPane().add(btnCancelar);
		
		comboBoxListarInstitutos.setEnabled(false);
		JButton btnRefresh = new JButton("Cargar Institutos");
		btnRefresh.addActionListener(new ActionListener() {
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent e) { 
				if(comboBoxListarInstitutos.isEnabled()==false) {
					// Inicialiar Comobo Box
					comboBoxListarInstitutos.setEnabled(true);
					comboBoxListarInstitutos.removeAllItems();
					String[] lista = iconIP.listarInstitutosArray();
					// Si hay registros 
					if(lista != null) {
						String primerItem = lista[0];
						for(String i : lista){
							//System.out.println(i);
							comboBoxListarInstitutos.addItem(i);					
						}			
						comboBoxListarInstitutos.setSelectedItem(primerItem);
						getContentPane().add(comboBoxListarInstitutos);
						institutoSeleccionado=false;
						
					} else {
						comboBoxListarInstitutos.addItem("Sin Datos");	
						comboBoxListarInstitutos.setEnabled(false);
					}
				}
				
				
			}
		});
		btnRefresh.setBounds(10, 6, 123, 21);
		getContentPane().add(btnRefresh);
		
		// Inicialiar Comobo Box
		/*String[] lista = iconIP.listarInstitutosArray();
		// Si hay registros 
		if(lista != null) {
			String primerItem = lista[0];
			for(String i : lista){
				System.out.println(i);
				comboBoxListarInstitutos.addItem(i);					
			}			
			comboBoxListarInstitutos.setSelectedItem(primerItem);
			getContentPane().add(comboBoxListarInstitutos);
			
		} else {
			comboBoxListarInstitutos.addItem("Sin Datos");	
			comboBoxListarInstitutos.setEnabled(false);
		}*/

		
	}
	//setearCurso(String nombreCurso, String desc, String duracion, int cantHoras, int cantCreditos, String url, Date fechaAlta);

	protected boolean crear(IControladorInstProg iconIP, JTextField textFieldNombre) {
		String nom = textFieldNombre.getText();
		boolean noExiste = false;
			
			if(institutoSeleccionado) {
				noExiste =  iconIP.nuevoCurso(nom);
				if(noExiste) {
					String desc = textFielDesc.getText();
					String duracion = textFielDuracion.getText();
					int canthoras = Integer.parseInt(textFieldHoras.getText());
					int creditos= Integer.parseInt(textFieldCreditos.getText());
					String url = textFieldUrl.getText();
					Date d = new Date();
					iconIP.setearCurso(nom, desc, duracion, canthoras,  creditos,  url, d);
					//combocursos
					
				}else {
					System.out.println("Ya Existe");
					textFieldNombre.setText("yaExiste");
		    		//JOptionPane.showMessageDialog(this, "Ese ya est� ingresado, fijate bien!","Swing Tester", JOptionPane.ERROR_MESSAGE);

				}				
			}else {
				JOptionPane.showMessageDialog(this, "Seleccionar Instituto","Swing Tester", JOptionPane.ERROR_MESSAGE);
			}
			


		
		return noExiste;

	}
	
	public void aVerSiMeDeja() {
		this.dispose();
		
		
	}

}
