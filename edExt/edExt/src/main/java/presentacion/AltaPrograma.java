package presentacion;

import java.awt.EventQueue;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.JInternalFrame;

import interfaces.IControladorInstProg;
import javax.swing.JTextField;

import excepciones.EmailExistenteException;
import excepciones.NickNameExistenteException;
import excepciones.ProgramaExisteException;

import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;

public class AltaPrograma extends JInternalFrame {

	private static final long serialVersionUID = 1L;
	private JTextField textNombre;
	private JTextField textDescripcion;
	private IControladorInstProg iconIP;
	private Date fechaInicio;
	private Date fechaFinal;
	private String nombre;
	private String descripcion;
	
	public AltaPrograma(IControladorInstProg iconIP) {
		this.iconIP = iconIP;
		
		setBounds(100, 100, 450, 300);
		setClosable(true);
		getContentPane().setLayout(null);
		
		textNombre = new JTextField();
		textNombre.setBounds(56, 50, 96, 19);
		getContentPane().add(textNombre);
		textNombre.setColumns(10);
		
		textDescripcion = new JTextField();
		textDescripcion.setBounds(222, 50, 96, 19);
		getContentPane().add(textDescripcion);
		textDescripcion.setColumns(10);
		
		JComboBox comboFechIniDia = new JComboBox();
		comboFechIniDia.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"}));
		comboFechIniDia.setBounds(41, 118, 55, 21);
		getContentPane().add(comboFechIniDia);
		
		JComboBox comboFechIniMes = new JComboBox();
		comboFechIniMes.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"}));
		comboFechIniMes.setBounds(116, 118, 55, 21);
		getContentPane().add(comboFechIniMes);
		
		JComboBox comboFechIniAnio = new JComboBox();
		comboFechIniAnio.setModel(new DefaultComboBoxModel(new String[] {"2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024"}));
		comboFechIniAnio.setBounds(193, 118, 55, 21);
		getContentPane().add(comboFechIniAnio);
		
		JComboBox comboFechFinDia = new JComboBox();
		comboFechFinDia.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"}));
		comboFechFinDia.setBounds(41, 171, 55, 21);
		getContentPane().add(comboFechFinDia);
		
		JComboBox comboFechFinMes = new JComboBox();
		comboFechFinMes.setModel(new DefaultComboBoxModel(new String[] {"1","2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"}));
		comboFechFinMes.setBounds(116, 171, 55, 21);
		getContentPane().add(comboFechFinMes);
		
		JComboBox comboFechaFinAnio = new JComboBox();
		comboFechaFinAnio.setModel(new DefaultComboBoxModel(new String[] {"2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024"}));
		comboFechaFinAnio.setBounds(193, 171, 55, 21);
		getContentPane().add(comboFechaFinAnio);
		
		JButton btnFechInicio = new JButton("Set FechaInicio");
		btnFechInicio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String diaSt=comboFechIniDia.getSelectedItem().toString();
				int dia= Integer.parseInt(diaSt);
				String mesSt= comboFechIniMes.getSelectedItem().toString();
				int mes = Integer.parseInt(mesSt);
				String anioSt = comboFechIniAnio.getSelectedItem().toString();
				int anio = Integer.parseInt(anioSt);
			    Calendar calendar = GregorianCalendar.getInstance();
			    calendar.set(Calendar.DAY_OF_MONTH, dia);
			    calendar.set(Calendar.MONTH, mes-1);
			    calendar.set(Calendar.YEAR, anio);
				fechaInicio = calendar.getTime();
				System.out.println(fechaInicio);
				
			}
		});
		btnFechInicio.setBounds(286, 118, 142, 21);
		getContentPane().add(btnFechInicio);
		
		JButton btnFechaFinal = new JButton("Set FechaFinal");
		btnFechaFinal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String diaSt=comboFechFinDia.getSelectedItem().toString();
				int dia= Integer.parseInt(diaSt);
				String mesSt= comboFechFinMes.getSelectedItem().toString();
				int mes = Integer.parseInt(mesSt);
				String anioSt = comboFechaFinAnio.getSelectedItem().toString();
				int anio = Integer.parseInt(anioSt);
			    Calendar calendar = GregorianCalendar.getInstance();
			    calendar.set(Calendar.DAY_OF_MONTH, dia);
			    calendar.set(Calendar.MONTH, mes-1);
			    calendar.set(Calendar.YEAR, anio);
			    fechaFinal = calendar.getTime();
				System.out.println(fechaFinal);
			}
		});
		btnFechaFinal.setBounds(286, 171, 142, 21);
		getContentPane().add(btnFechaFinal);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(71, 27, 45, 13);
		getContentPane().add(lblNombre);
		
		JLabel lblDesc = new JLabel("Descripcion");
		lblDesc.setBounds(240, 27, 63, 13);
		getContentPane().add(lblDesc);
		
		JButton btnCrear = new JButton("Crear");
		btnCrear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nombre = textNombre.getText();
				descripcion = textDescripcion.getText();
                try {
                	iconIP.altaPrograma(nombre, descripcion, fechaInicio, fechaFinal);
                } catch (ProgramaExisteException e1) {
                    JOptionPane.showMessageDialog(null ,  e1.getMessage()  , "Alta de Programa", JOptionPane.ERROR_MESSAGE);
                	}
			}
		});
		btnCrear.setBounds(43, 229, 85, 21);
		getContentPane().add(btnCrear);
		this.setTitle("Agregar Programa");
		

	}
}
