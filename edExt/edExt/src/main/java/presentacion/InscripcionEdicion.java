package presentacion;

import javax.swing.Action;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("unused")
public class InscripcionEdicion extends JInternalFrame {
	private static final long serialVersionUID = 1L;
	private void cancelar() {
		this.dispose();
	}
	
	public InscripcionEdicion(IControladorInstProg icip,IControladorUsuario icu) {
		setTitle("Inscripcion a Edicion de Curso");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblarriba = new JLabel("Inscripciones");
		lblarriba.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblarriba.setBounds(150, 15, 120, 15);
		getContentPane().add(lblarriba);
		
		JLabel lblinstituto = new JLabel("Instituto");
		lblinstituto.setHorizontalAlignment(SwingConstants.CENTER);
		lblinstituto.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblinstituto.setBounds(30, 60, 100, 14);
		getContentPane().add(lblinstituto);
		
		JLabel lblcurso = new JLabel("Curso");
		lblcurso.setHorizontalAlignment(SwingConstants.CENTER);
		lblcurso.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblcurso.setBounds(30, 90, 100, 14);
		getContentPane().add(lblcurso);
		
		JLabel lbledicion = new JLabel("Edicion");
		lbledicion.setHorizontalAlignment(SwingConstants.CENTER);
		lbledicion.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbledicion.setBounds(30, 120, 100, 14);
		getContentPane().add(lbledicion);
		
		JLabel lblestudiante = new JLabel("Estudiante");
		lblestudiante.setHorizontalAlignment(SwingConstants.CENTER);
		lblestudiante.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblestudiante.setBounds(30, 150, 100, 14);
		getContentPane().add(lblestudiante);
		
		JComboBox<String> comboBoxListarInstitutos = new JComboBox<String>();		
		comboBoxListarInstitutos.setBounds(126, 57, 230, 22);
		getContentPane().add(comboBoxListarInstitutos);
		
		
		JComboBox<String> comboBoxListarCursos = new JComboBox<String>();
		comboBoxListarCursos.setEnabled(false);
		comboBoxListarCursos.setBounds(126, 87, 230, 22);
		getContentPane().add(comboBoxListarCursos);
		
		JComboBox<String> comboBoxListarEdiciones = new JComboBox<String>();
		comboBoxListarEdiciones.setEnabled(false);
		comboBoxListarEdiciones.setBounds(126, 117, 230, 22);
		getContentPane().add(comboBoxListarEdiciones);
		
		JComboBox<String> comboBoxListarEstudiantes = new JComboBox<String>();
		comboBoxListarEstudiantes.setBounds(126, 147, 230, 22);
		getContentPane().add(comboBoxListarEstudiantes);
		
		JButton btnCancelar = new JButton("Abortar");
		btnCancelar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				comboBoxListarInstitutos.removeAllItems();
				comboBoxListarCursos.removeAllItems();
				comboBoxListarEdiciones.removeAllItems();
				cancelar();
			}
		});
		
		btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnCancelar.setBounds(243, 215, 100, 23);
		getContentPane().add(btnCancelar);
		
		JButton btnAceptar = new JButton("Efectuar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnAceptar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(comboBoxListarInstitutos.getSelectedItem()!=null) {
					if(comboBoxListarCursos.getSelectedItem()!=null) {
						if(comboBoxListarEdiciones.getSelectedItem()!=null) {
							if(comboBoxListarEstudiantes.getSelectedItem()!=null) {
								String instituto = (String) comboBoxListarInstitutos.getSelectedItem();
								String curso = (String) comboBoxListarCursos.getSelectedItem();
								String edicion = (String) comboBoxListarEdiciones.getSelectedItem();
								String estudiante = (String) comboBoxListarEstudiantes.getSelectedItem();
								icip.realizarInscripcion(instituto,curso,edicion,estudiante,icu);
								System.out.println("Realizada Inscripcion :"+instituto+" "+curso+" "+edicion+" "+estudiante);
							}
						}
					}
				}
				
			}
		});
		btnAceptar.setEnabled(true);
		btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAceptar.setBounds(110, 215, 100, 23);
		getContentPane().add(btnAceptar);
		////////////////////Komboboxz INTERACTIONS <3///////////////////////
		comboBoxListarInstitutos.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
			comboBoxListarInstitutos.removeAllItems();
			comboBoxListarCursos.removeAllItems();
			comboBoxListarEdiciones.removeAllItems();
			String[] listainstitutos = icip.listarInstitutosArray();
			if(listainstitutos!=null) {
				String primerItem = listainstitutos[0];
				for(String i : listainstitutos){
					System.out.println("Agregando item : "+i+" en comboxInstituto");
					comboBoxListarInstitutos.addItem(i);					
				}			
				comboBoxListarInstitutos.setSelectedItem(primerItem);
			} else {
				comboBoxListarInstitutos.removeAllItems();
				comboBoxListarInstitutos.addItem("Sin institutos");
				comboBoxListarCursos.setEnabled(false);
				comboBoxListarEdiciones.setEnabled(false);

			}
			System.out.println("inscripcion edc :Listo institutos");
			}
		});
		
		comboBoxListarCursos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent entrastecursos) {
				String institutoaux = (String) comboBoxListarInstitutos.getSelectedItem();
				comboBoxListarCursos.removeAllItems();
				comboBoxListarEdiciones.removeAllItems();
				String[] listacursos = icip.listarNombreCursos(institutoaux);
				if(listacursos.length > 0) {
					String primerItem = listacursos[0];
					for(String i : listacursos){
						System.out.println("Agregando item : "+i+" en comboxCursos");
						comboBoxListarCursos.addItem(i);					
					}			
					comboBoxListarCursos.setSelectedItem(primerItem);
					getContentPane().add(comboBoxListarCursos);		
					comboBoxListarCursos.setEnabled(true);
				} else { 
					comboBoxListarCursos.removeAllItems();
					comboBoxListarCursos.addItem("Sin Cursos");	
					comboBoxListarCursos.setEnabled(false);
				}
			}
		});
		
		comboBoxListarEdiciones.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent entrasteediciones) {
				comboBoxListarEdiciones.removeAllItems();
				String cursoaux= (String) comboBoxListarCursos.getSelectedItem();
				String[] listaediciones = icip.listarEdicionesdeCurso(cursoaux);
				if(listaediciones.length > 0) {
					String ultimoitem=listaediciones[0];
					for(String i : listaediciones){
 						comboBoxListarEdiciones.addItem(i);
					}			
					comboBoxListarEdiciones.setSelectedItem(ultimoitem);
					//por defecto seleccionamos la ultima edicion como la vigente,no hay opcion de seleccionar otra
					comboBoxListarEdiciones.setEnabled(true); 
				} else { 
					comboBoxListarEdiciones.removeAllItems();
					comboBoxListarEdiciones.addItem("Sin Edicion vigente");	
					comboBoxListarEdiciones.setEnabled(true);
				}
				
			}
		});
		comboBoxListarEstudiantes.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent entrasteediciones) {
				comboBoxListarEstudiantes.removeAllItems();
				List<String> listaNicks = icu.listarEstudiantes();
				if(listaNicks.size() > 0) {
					List<String> listaNombres = icu.listarEstudiantes();
					String[] listaNombresArray = new String[listaNombres.size()];
					listaNombresArray = listaNombres.toArray(listaNombresArray);
					String primeritem = listaNombresArray[0];
					for(String i : listaNombresArray){
						System.out.println("Agregando item : "+i+" en comboxEstudiantes :");
						comboBoxListarEstudiantes.addItem(i);					
					}			
					comboBoxListarEstudiantes.setSelectedItem(primeritem);
					comboBoxListarEstudiantes.setEnabled(true);
				} else { 
					comboBoxListarEstudiantes.removeAllItems();
					comboBoxListarEstudiantes.addItem("Sin Estudiantes");	
					comboBoxListarEstudiantes.setEnabled(false);
				}
			}
		});
	}
}
