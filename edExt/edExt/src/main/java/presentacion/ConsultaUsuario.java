package presentacion;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JInternalFrame;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import com.toedter.calendar.JDateChooser;

import datatypes.DtEdicion;
import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;
import logica.Curso;
import logica.Docente;
import logica.Edicion;
import logica.Estudiante;
import logica.InscripcionEdc;
import logica.InscripcionPF;
import logica.Instituto;
import logica.Programa;
import logica.Usuario;

import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class ConsultaUsuario extends JInternalFrame {
	
	private static final long serialVersionUID = 1L;
	
	private IControladorUsuario iConUsu;
	private IControladorInstProg iConIns;
	
	private JComboBox<String> comboBoxUsuario; 
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtCorreo;
	private JDateChooser dateChooserFN;
	private JTextField textFieldMensaje;
	private JComboBox<String> comboBoxEdiciones;
	private JComboBox<String> comboBoxProgramas;
	private JTextField textFieldTipo;
	private JLabel txtProgramas;
	private JTextArea textAreaInformacion;


	
	/**
	 * Launch the application.
	 *//*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConsultaUsuario frame = new ConsultaUsuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 * @param iConUsu 
	 */
	public ConsultaUsuario(final IControladorUsuario iConUsu, final IControladorInstProg iConIns) {
		this.iConUsu = iConUsu;
		this.iConIns = iConIns;
		setTitle("Consulta de Usuario");
		setClosable(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(12, 23, 56, 16);
		getContentPane().add(lblUsuario);
		
		comboBoxUsuario = new JComboBox<String>();
		comboBoxUsuario.setEnabled(true);
		comboBoxUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rellenarDatos();
			}
		});
		comboBoxUsuario.setBounds(75, 20, 149, 22);
		getContentPane().add(comboBoxUsuario);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(12, 52, 56, 16);
		getContentPane().add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(12, 81, 56, 16);
		getContentPane().add(lblApellido);
		
		JLabel lblCorreo = new JLabel("Correo");
		lblCorreo.setBounds(12, 110, 56, 16);
		getContentPane().add(lblCorreo);
		
		txtNombre = new JTextField();
		txtNombre.setEditable(false);
		txtNombre.setBounds(75, 49, 149, 22);
		getContentPane().add(txtNombre);
		txtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setEditable(false);
		txtApellido.setBounds(75, 78, 149, 22);
		getContentPane().add(txtApellido);
		txtApellido.setColumns(10);
		
		txtCorreo = new JTextField();
		txtCorreo.setEditable(false);
		txtCorreo.setBounds(75, 107, 149, 22);
		getContentPane().add(txtCorreo);
		txtCorreo.setColumns(10);
		
		JLabel lblFechaDeNacimiento = new JLabel("FechanNac.");
		lblFechaDeNacimiento.setBounds(12, 147, 127, 15);
		getContentPane().add(lblFechaDeNacimiento);
		
		dateChooserFN = new JDateChooser();
		dateChooserFN.setBounds(85, 142, 139, 19);
		getContentPane().add(dateChooserFN);
		
		
		textFieldMensaje = new JTextField();
		textFieldMensaje.setEnabled(false);
		textFieldMensaje.setEditable(false);
		textFieldMensaje.setBounds(0, 242, 240, 22);
		getContentPane().add(textFieldMensaje);
		textFieldMensaje.setColumns(10);
		textFieldMensaje.setDisabledTextColor(Color.BLACK);
		
		comboBoxEdiciones = new JComboBox();
		comboBoxEdiciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listarComboBoxEdicion();
			}
		});
		comboBoxEdiciones.setBounds(85, 175, 139, 22);
		getContentPane().add(comboBoxEdiciones);
		
		comboBoxProgramas = new JComboBox();
		comboBoxProgramas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		comboBoxProgramas.setBounds(85, 210, 139, 22);
		getContentPane().add(comboBoxProgramas);
		
		JLabel txtEdiciones = new JLabel("Ediciones");
		txtEdiciones.setBounds(12, 178, 56, 16);
		getContentPane().add(txtEdiciones);
		
		txtProgramas = new JLabel("Programas");
		txtProgramas.setBounds(12, 213, 71, 16);
		getContentPane().add(txtProgramas);
		
		textFieldTipo = new JTextField();
		textFieldTipo.setEditable(false);
		textFieldTipo.setBounds(293, 20, 116, 22);
		getContentPane().add(textFieldTipo);
		textFieldTipo.setColumns(10);
		
		textAreaInformacion = new JTextArea();
		textAreaInformacion.setBounds(236, 49, 173, 202);
		getContentPane().add(textAreaInformacion);
	}
	
	public void inicializarComboBoxes() {
		DefaultComboBoxModel<String> nickUsuarios = new DefaultComboBoxModel<String>(iConUsu.listarUsuarios());
		comboBoxUsuario.setModel(nickUsuarios);
	}
	
	public void rellenarDatos() {
		Usuario u = iConUsu.seleccionarUsuario(this.comboBoxUsuario.getSelectedItem().toString());
		textAreaInformacion.setText("");
		txtNombre.setText(u.getNombre());
		txtApellido.setText(u.getApellido());
		txtCorreo.setText(u.getCorreo());
		dateChooserFN.setDate(u.getFechaNacimiento());
		dateChooserFN.setEnabled(false);
		if(u.getEsDocente()) {
				textFieldTipo.setText("DOCENTE");
				comboBoxProgramas.setVisible(false);
				txtProgramas.setVisible(false);
				List<String> edicionFin = new ArrayList<String>();
				Instituto i = iConUsu.seleccionarUnInstituto(((Docente)u).getNombreInsituto());
				List<Curso> cursos = i.getCursos();
				if(cursos != null) {
					for(Curso c: cursos) {
						List<Edicion> ediciones = c.getEdiciones();
						if(ediciones != null) {
							for(Edicion e: ediciones)
							{
								List<Docente> docentes = e.getListaDocente();
								if(docentes != null) {
									for(Usuario d: docentes) {
										if(d.getNickName().equals(u.getNickName())) {
											edicionFin.add(e.getNombre());
										}
									}
								}
							}
						}
					}
				}
				int j = 0;
				String[] edicionC = new String[edicionFin.size()];
				for(String e: edicionFin) {
					edicionC[j] = e;
					j++;
				}
				DefaultComboBoxModel<String> nickUsuarios = new DefaultComboBoxModel<String>(edicionC);
				comboBoxEdiciones.setModel(nickUsuarios);
			}
			else {
				comboBoxProgramas.setVisible(true);
				txtProgramas.setVisible(true);
				//Mostrar ediciones
				textFieldTipo.setText("ESTUDIANTE");
				//List<String> programaFin = new ArrayList<String>();
				List<String> inscripciones = new ArrayList<String>();
				String[] institutos = iConIns.listarInstitutos();
				if(!institutos.toString().isEmpty()) {
					for(String ins: institutos) {
						String[] cursos = iConIns.listarNombreCursos(ins);
						if(!cursos.toString().isEmpty()) {
							for(String cur: cursos) {
								String[] ediciones = iConIns.listarEdicionesdeCurso(cur);
								if(!ediciones.toString().isEmpty()) {
									for(String ed: ediciones) {
										if(inscripciones.isEmpty()) {
											String[] aux = iConUsu.seleccionarInscripcionED(u.getNickName());
											for(String i:aux) {
												inscripciones.add(i);
											}
										}									
									}
								}
								else {
									textFieldMensaje.setText("No hay ediciones.");
								}
							}
						}
						else {
							textFieldMensaje.setText("No hay cursos.");
						}
					}
				}
				else {
					textFieldMensaje.setText("No hay institutos.");
				}
				int i = 0;
				String[] edicionC = new String[inscripciones.size()];
				for(String e: inscripciones) {
					edicionC[i] = e;
					i++;
				}
				DefaultComboBoxModel<String> nomEdicion = new DefaultComboBoxModel<String>(edicionC);
				comboBoxEdiciones.setModel(nomEdicion);
			
				//Mostrar Programas
				List<InscripcionPF> inscripcionesPF = ((Estudiante)u).getInscripcionesPF();
				if(inscripcionesPF != null) {
					List<String> programa = new ArrayList<String>();
					for(InscripcionPF ins: inscripcionesPF) {
						programa.add(ins.getPrograma().getNombre());
					}
					int j = 0;
					String[] prog = new String[programa.size()];
					for(String p: programa) {
						prog[j] = p;
						j++;
					}
					DefaultComboBoxModel<String> nomPrograma = new DefaultComboBoxModel<String>(prog);
					comboBoxProgramas.setModel(nomPrograma);
				}
			}
		}
	
	
	public void listarComboBoxEdicion() {
		Usuario u = iConUsu.seleccionarUsuario(comboBoxUsuario.getSelectedItem().toString());
		List<InscripcionEdc> ediciones = u.getInscripciones();
		if(ediciones == null) {
			textAreaInformacion.setText("No hay datos.\n");
			comboBoxEdiciones.setEnabled(false);
			comboBoxProgramas.setEnabled(false);
		}
		else {
			comboBoxEdiciones.setEnabled(true);
			comboBoxProgramas.setEnabled(true);
			textAreaInformacion.setText("Datos de Ediciones de Curso.\n");
			Edicion edicion = iConIns.seleccionarEdicion((String)comboBoxEdiciones.getSelectedItem());
			textAreaInformacion.append("Nombre: " + edicion.getNombre() + "\n");
			textAreaInformacion.append("Fecha de Inicio: " + edicion.getFechaI() + "\n");
			textAreaInformacion.append("Fecha de Finalizacion: " + edicion.getFechaF() + "\n");
			textAreaInformacion.append("Cupo: " + edicion.getCupo() + "\n") ;
			textAreaInformacion.append("Fecha de Publicacion: " + edicion.getFechaPub() + "\n");
			textAreaInformacion.append("\nLista de Docentes:\n");
		
			List<Docente> lista = edicion.getListaDocente();
			System.out.println("Cantidad de Docentes: " + lista.size());
			for (Docente l : lista) {
				System.out.println(l.getNickName());	
				textAreaInformacion.append(l.getNickName() + "\n");
			}
		}
	}
}