package presentacion;

import java.awt.EventQueue;

import interfaces.Fabrica;

import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;
import java.awt.Toolkit;
import java.awt.SystemColor;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("unused")
public class Principal {

	private JFrame frame;	
	private AltaUsuario altaUsuarioInternalFrame;
	private ConsultaEdicionCurso consultaEdicionCurso;
	private AltaInstituto altaInstitutoIFrame;
	private AltaCurso altaCursoIFrame;
	private ConsultaCurso consultaCursoIFrame;
	private AltaEdicionCurso altaEdicionCursoIFrame;
	private InscripcionEdicion inscripcionEdicionIFrame;
	private AltaPrograma altaProgramaIFrame;
	private ConsultaUsuario consultaUsuarioIFrame;
	private ModificarUsuario modificarUsuarioIFrame;
	private ConsultaProgramaFormacion consultaProgramaFormacionIFrame;
	
	private static EntityManagerFactory emf;
	private static EntityManager em;


	public static void main(String[] args) {
		// Esto creo que no lo comente yo (Wueisman), cualquier cosa descomentenlo
		
		emf = Persistence.createEntityManagerFactory("edExt");
		em = emf.createEntityManager();
		

		System.out.println("Se abrio el programa ");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal window = new Principal();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	@SuppressWarnings("unused")
	public Principal() {
		initialize();
		
		Fabrica fabrica = Fabrica.getInstancia();
		IControladorInstProg iCip = fabrica.getIControladorIP();
		IControladorUsuario iConUs = fabrica.getIControladorU();
		
		Dimension desktopSize = frame.getSize();
		Dimension jInternalFrameSize;
		
		//internalFrame AltaUsuario
		altaUsuarioInternalFrame = new AltaUsuario(iConUs, iCip);
		altaUsuarioInternalFrame.setSize(609, 421);
		jInternalFrameSize = altaUsuarioInternalFrame.getSize();
		altaUsuarioInternalFrame.setLocation(0,0);
		altaUsuarioInternalFrame.setVisible(false);
		frame.getContentPane().add(altaUsuarioInternalFrame);

		//internalFrame ConsultaEdicionCurso
		consultaEdicionCurso = new ConsultaEdicionCurso(iCip,iConUs);
		consultaEdicionCurso.setSize(609,421);
		jInternalFrameSize = consultaEdicionCurso.getSize();
		consultaEdicionCurso.setLocation(0,0);
		consultaEdicionCurso.setVisible(false);
		frame.getContentPane().add(consultaEdicionCurso);
		
		// internalFrame AltaInstituto
		altaInstitutoIFrame= new AltaInstituto(iConUs, iCip);
		altaInstitutoIFrame.setSize(609,421);
		jInternalFrameSize=altaInstitutoIFrame.getSize();
		altaInstitutoIFrame.setLocation(0, 0);
		frame.getContentPane().add(altaInstitutoIFrame);
		
		//internalFrame AltaCurso
		altaCursoIFrame = new AltaCurso(iConUs, iCip);
		altaCursoIFrame.setSize(609,421);
		jInternalFrameSize=altaCursoIFrame.getSize();
		altaCursoIFrame.setLocation(0,0);
		frame.getContentPane().add(altaCursoIFrame);
		
		//internalFrame ConsultaCurso
		consultaCursoIFrame = new ConsultaCurso(iCip);
		consultaCursoIFrame.setSize(609,421);
		jInternalFrameSize = consultaCursoIFrame.getSize();
		consultaCursoIFrame.setLocation(0,0);
		frame.getContentPane().add(consultaCursoIFrame);
		
		//internalFrame AltaEdicionCurso
		altaEdicionCursoIFrame = new AltaEdicionCurso(iCip, iConUs);
		altaEdicionCursoIFrame.setSize(630,500);
		jInternalFrameSize = altaEdicionCursoIFrame.getSize();
		altaEdicionCursoIFrame.setLocation(0,0);
		altaEdicionCursoIFrame.setVisible(false);
		frame.getContentPane().add(altaEdicionCursoIFrame);
		
		//internalFrame InscripcionEdicion
		inscripcionEdicionIFrame = new InscripcionEdicion(iCip, iConUs);
		inscripcionEdicionIFrame.setSize(630,500);
		jInternalFrameSize = inscripcionEdicionIFrame.getSize();
		inscripcionEdicionIFrame.setLocation(0,0);
		inscripcionEdicionIFrame.setVisible(false);
		frame.getContentPane().add(inscripcionEdicionIFrame);

		//internalFrame AltaPrograma
		altaProgramaIFrame = new AltaPrograma(iCip);
		altaProgramaIFrame.setSize(630,500);
		jInternalFrameSize = altaProgramaIFrame.getSize();
		altaProgramaIFrame.setLocation(0,0);
		altaEdicionCursoIFrame.setVisible(false);
		frame.getContentPane().add(altaProgramaIFrame);	
		
		//internalFrame ConsultaUsuario
		consultaUsuarioIFrame = new ConsultaUsuario(iConUs, iCip);
		consultaUsuarioIFrame.setSize(609, 421);
		jInternalFrameSize = consultaUsuarioIFrame.getSize();
		consultaUsuarioIFrame.setLocation(0,0);
		consultaUsuarioIFrame.setVisible(false);
		frame.getContentPane().add(consultaUsuarioIFrame);
		
		//internalFrame ModificarUsuario
		modificarUsuarioIFrame = new ModificarUsuario(iConUs);
		modificarUsuarioIFrame.setSize(609, 421);
		jInternalFrameSize = modificarUsuarioIFrame.getSize();
		modificarUsuarioIFrame.setLocation(0,0);
		modificarUsuarioIFrame.setVisible(false);
		frame.getContentPane().add(modificarUsuarioIFrame);
		
		//internalFrame ComsultaProgramaFormacion
		consultaProgramaFormacionIFrame = new ConsultaProgramaFormacion(iCip);
		consultaProgramaFormacionIFrame.setSize(609, 421);
		jInternalFrameSize = consultaProgramaFormacionIFrame.getSize();
		consultaProgramaFormacionIFrame.setLocation(0,0);
		consultaProgramaFormacionIFrame.setVisible(false);
		frame.getContentPane().add(consultaProgramaFormacionIFrame);
	}


	private void initialize() {
		
		frame = new JFrame();
		frame.getContentPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		frame.getContentPane().setBackground(SystemColor.inactiveCaption);
		frame.setForeground(Color.GRAY);
		frame.setTitle("edEXT");
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(Principal.class.getResource("/img/icons8-spider-man-head-30.png")));
		frame.setBackground(Color.LIGHT_GRAY);
		frame.setBounds(100, 100, 813, 586);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Usuarios");
		mnNewMenu.setBackground(Color.GRAY);
		mnNewMenu.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmAltaUsuario = new JMenuItem("Alta");
		mntmAltaUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				altaUsuarioInternalFrame.setVisible(true);
				altaUsuarioInternalFrame.iniciarlizarComboBoxes();
			}
		});
		mntmAltaUsuario.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		mnNewMenu.add(mntmAltaUsuario);
		
		JMenuItem mntmNewMenuModifUsuario = new JMenuItem("Modificar");
		mntmNewMenuModifUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				modificarUsuarioIFrame.setVisible(true);
				modificarUsuarioIFrame.inicializarComboBoxes();
			}
		});
		mnNewMenu.add(mntmNewMenuModifUsuario);
		
		JMenuItem mntmConsultUsuario = new JMenuItem("Consultar");
		mntmConsultUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				consultaUsuarioIFrame.inicializarComboBoxes();
				consultaUsuarioIFrame.setVisible(true);
			}
		});
		mntmConsultUsuario.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
		mnNewMenu.add(mntmConsultUsuario);
		
		JMenu mnNewMenu_2 = new JMenu("Instituto");
		mnNewMenu_2.setBackground(Color.GRAY);
		mnNewMenu_2.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menuBar.add(mnNewMenu_2);
		
		JMenuItem mntmAltaInstituto = new JMenuItem("Alta");
		mntmAltaInstituto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				altaInstitutoIFrame.setVisible(true);
			}
		} );
		mnNewMenu_2.add(mntmAltaInstituto);
		
		JMenu mnNewMenu_1 = new JMenu("Curso");
		mnNewMenu_1.setBackground(Color.GRAY);
		mnNewMenu_1.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		mnNewMenu_1.setForeground(Color.BLACK);
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmAltaCurso = new JMenuItem("Alta");
		mnNewMenu_1.add(mntmAltaCurso);
		mntmAltaCurso.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		mntmAltaCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				altaCursoIFrame.setVisible(true);
				//altaCursoIFrame.iniciarlizarComboBoxes();
			}			
		});
		
		JMenuItem mntmConsultaCurso = new JMenuItem("Consulta");
		mnNewMenu_1.add(mntmConsultaCurso);
		mntmConsultaCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				consultaCursoIFrame.setVisible(true);
				//altaCursoIFrame.iniciarlizarComboBoxes();
			}	
		});
		
		JMenuItem mntmAltaEdicionCurso = new JMenuItem("Alta edicion de curso");
		mntmAltaEdicionCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				altaEdicionCursoIFrame.setVisible(true);
				//altaEdicionCursoIFrame.iniciarlizarComboBoxes();
			}
		});
		mnNewMenu_1.add(mntmAltaEdicionCurso);
		mntmAltaEdicionCurso.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		
		JMenuItem mntmConsultaEdicCurso = new JMenuItem("Consulta edicion de curso");
		mntmConsultaEdicCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				consultaEdicionCurso.setVisible(true);
			}
		});
		mnNewMenu_1.add(mntmConsultaEdicCurso);
		
		JMenu mnNewMenu_3 = new JMenu("Prog. de formaci\u00F3n");
		mnNewMenu_3.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menuBar.add(mnNewMenu_3);
		
		JMenuItem mntmCrearProgDeFormacion = new JMenuItem("Crear nuevo");
		mntmCrearProgDeFormacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				altaProgramaIFrame.setVisible(true);
			}
		});
		mnNewMenu_3.add(mntmCrearProgDeFormacion);
		
		JMenuItem mntmAgregarCursoAprogDeFormacion = new JMenuItem("Agregar cursos");
		mnNewMenu_3.add(mntmAgregarCursoAprogDeFormacion);
		
		JMenuItem mntmConsultarProgFormacion = new JMenuItem("Consultar");
		mntmConsultarProgFormacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				consultaProgramaFormacionIFrame.setVisible(true);
				consultaProgramaFormacionIFrame.inicializarComboBoxPF();
			}
		});
		mnNewMenu_3.add(mntmConsultarProgFormacion);
		frame.getContentPane().setLayout(null);
		
		////////////////////////////MENU DE INSCRIPCIONES///////////////////////
		JMenu mnInscripciones = new JMenu("Inscripciones");
		mnInscripciones.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menuBar.add(mnInscripciones);
		
		JMenuItem mntmNuevaInscripcion = new JMenuItem("Nueva inscripcion");
		mntmNuevaInscripcion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inscripcionEdicionIFrame.setVisible(true);
			}
		});
		mnInscripciones.add(mntmNuevaInscripcion);
				
		JMenuItem mntmConsultaInscripcion = new JMenuItem("ConsultarInscripcion");
		mnInscripciones.add(mntmConsultaInscripcion);
		frame.getContentPane().setLayout(null);
	}
}
