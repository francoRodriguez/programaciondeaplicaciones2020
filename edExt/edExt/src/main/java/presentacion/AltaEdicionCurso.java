package presentacion;

import java.awt.EventQueue;
import java.awt.Panel;

import javax.swing.JInternalFrame;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;
import logica.Docente;
import logica.Usuario;

import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JTextArea;
import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;

@SuppressWarnings("unused")
public class AltaEdicionCurso extends JInternalFrame {

private static final long serialVersionUID = 1L;
private String[] listaCursos;
private List<String> listaDocentesU;
private List<String> listaDocentes = new ArrayList<String>();
private List<Docente> listaDocentesParaAgregar = new ArrayList<Docente>();
	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 * @param iCip 
	 */
	@SuppressWarnings("rawtypes")
	public AltaEdicionCurso(final IControladorInstProg iconIP, final IControladorUsuario iconU) {
		
		setResizable(true);
		setMaximizable(true);
		setClosable(true);
		setTitle("Alta Edicion Curso");
		setBounds(100, 100, 623, 509);
		getContentPane().setLayout(null);
		
		
		JLabel lblNewLabel = new JLabel("Seleccionar Instituto:");
		lblNewLabel.setBounds(12, 13, 132, 16);
		getContentPane().add(lblNewLabel);
		

		JComboBox comboInstitutos = new JComboBox();
		comboInstitutos.setBounds(57, 45, 295, 21);
		getContentPane().add(comboInstitutos);
		
		JComboBox combocursos = new JComboBox();
		combocursos.setBounds(57, 79, 295, 21);
		getContentPane().add(combocursos);
		

		comboInstitutos.setEnabled(false);
		JButton btnInstitutos = new JButton(",");
		btnInstitutos.addActionListener(new ActionListener() {
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent e) {
				if(comboInstitutos.isEnabled()==false) {
					// Inicialiar Comobo Box
					comboInstitutos.setEnabled(true);
					comboInstitutos.removeAllItems();
					String[] lista = iconIP.listarInstitutosArray();					
					// Si hay registros 
					if(lista != null) {
						String primerItem = lista[0];
						for(String i : lista){
							System.out.println(i);
							comboInstitutos.addItem(i);					
						}			
						comboInstitutos.setSelectedItem(primerItem);
						getContentPane().add(comboInstitutos);
						
					} else {
						comboInstitutos.addItem("Sin Datos");	
						comboInstitutos.setEnabled(false);
					}
				}
				
				
			}
		});
		btnInstitutos.setForeground(Color.WHITE);
		btnInstitutos.setIcon(new ImageIcon(ConsultaCurso.class.getResource("/img/Recargar.png")));
		btnInstitutos.setBounds(10, 42, 37, 27);
		getContentPane().add(btnInstitutos);
		
		combocursos.setEnabled(false);
		JButton btnSeleccionarInstituto = new JButton("Seleccionar Instituto");
		btnSeleccionarInstituto.addActionListener(new ActionListener() {
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent e) {
				listaCursos = iconIP.listarNombreCursos((String) comboInstitutos.getSelectedItem());
				if(listaCursos.length!=0 && listaCursos!=null) {
					combocursos.setEnabled(true);
					combocursos.removeAllItems();
					for(String i : listaCursos){
						System.out.println(i);
						combocursos.addItem(i);					
					}
				}else {
					combocursos.addItem("Sin Datos");	
				}
				comboInstitutos.setEnabled(false);
			}
		});
		btnSeleccionarInstituto.setBounds(399, 43, 183, 57);
		getContentPane().add(btnSeleccionarInstituto);
		
		JTextArea textAreaNombreEdicion = new JTextArea();
		textAreaNombreEdicion.setBounds(55, 142, 297, 22);
		getContentPane().add(textAreaNombreEdicion);
		
		JLabel lblNombreEdicion = new JLabel("Nombre Edicion");
		lblNombreEdicion.setBounds(55, 113, 183, 16);
		getContentPane().add(lblNombreEdicion);
		
		JLabel lblFechaDeInicio = new JLabel("Fecha de Inicio");
		lblFechaDeInicio.setBounds(57, 176, 183, 16);
		getContentPane().add(lblFechaDeInicio);
		
		JDateChooser dateChooserInicio = new JDateChooser();
		dateChooserInicio.setDateFormatString("d-MM-yyyy");
		dateChooserInicio.setBounds(57, 205, 100, 22);
		getContentPane().add(dateChooserInicio);
		
		JDateChooser dateChooserFin = new JDateChooser();
		dateChooserFin.setDateFormatString("d-MM-yyyy");
		dateChooserFin.setBounds(181, 205, 100, 22);
		getContentPane().add(dateChooserFin);
			
		JDateChooser dateChooserPublicacion = new JDateChooser();
		dateChooserPublicacion.setDateFormatString("d-MM-yyyy");
		dateChooserPublicacion.setBounds(181, 268, 100, 22);
		getContentPane().add(dateChooserPublicacion);
		
		JTextArea txtAreaCupo = new JTextArea();
		txtAreaCupo.setBounds(57, 269, 100, 22);
		getContentPane().add(txtAreaCupo);		
		
		JLabel lblFechaDeFin = new JLabel("Fecha de Fin");
		lblFechaDeFin.setBounds(181, 177, 183, 16);
		getContentPane().add(lblFechaDeFin);
		
		JLabel lblCupo = new JLabel("Cupo");
		lblCupo.setBounds(57, 240, 183, 16);
		getContentPane().add(lblCupo);
		
		
		JComboBox comboDocentes = new JComboBox();
		comboDocentes.setEnabled(false);
		comboDocentes.setBounds(57, 357, 295, 21);
		getContentPane().add(comboDocentes);
		
		JLabel lblDocentes = new JLabel("Docentes");
		lblDocentes.setBounds(57, 316, 183, 16);
		getContentPane().add(lblDocentes);
				
		JLabel lblListaDe = new JLabel("Lista de Docentes");
		lblListaDe.setBounds(399, 113, 183, 16);
		getContentPane().add(lblListaDe);
		
		JButton btnAgregarDocente = new JButton("Agregar Docente");
		btnAgregarDocente.addActionListener(new ActionListener() {
			@SuppressWarnings("null")
			public void actionPerformed(ActionEvent e) {
				// SELECCIONO EL DOCENTE QUE ESTA LISTADO Y LO AGREGO A LA LISTA DE DONCENTES
				String docente = (String) comboDocentes.getSelectedItem();				
				
				// Si el doncente ya esta en la lista no lo agrego.
				if (listaDocentes.contains(docente)) {
					//JOptionPane.showMessageDialog(this, "El Docente "+docente+" ya esta agregado", "Alta Edicion Curso", JOptionPane.INFORMATION_MESSAGE);
					System.out.println("No se puede agregar el Docente "+docente+" porque ya esta agregado");
				} else {
					listaDocentes.add(docente);
					
					for(String d: listaDocentes) {
						Docente doc = iconU.seleccionarDocente(d);
						listaDocentesParaAgregar.add(doc);
					}	
				}					
				
				updateTextTareaDocentes(listaDocentes);
			}
		});
		btnAgregarDocente.setBounds(57, 389, 295, 25);
		getContentPane().add(btnAgregarDocente);
		
		JButton btnListarDocentes = new JButton("Listar Docentes");
		btnListarDocentes.addActionListener(new ActionListener() {
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent arg0) {
				listaDocentesU = iconU.listarDocentes();
				if(!listaDocentesU.isEmpty()) {
					for(String i : listaDocentesU){
						System.out.println(i);
						comboDocentes.addItem(i);
					}					
				} else {
					String mensaje = "No hay Docentes en el sistema";
					System.out.println(mensaje);
					comboDocentes.addItem(mensaje);
				}
				
				comboDocentes.setEnabled(true);
			}
		});
		btnListarDocentes.setBounds(169, 312, 183, 25);
		getContentPane().add(btnListarDocentes);
		
		JLabel lblFechaDePub = new JLabel("Fecha de Publicaci\u00F3n");
		lblFechaDePub.setBounds(181, 240, 183, 16);
		getContentPane().add(lblFechaDePub);		

		
		JButton btnDarAltaEdicion = new JButton("Dar Alta Edicion Curso");
		btnDarAltaEdicion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
								
				String nombreInstit 	= (String) comboInstitutos.getSelectedItem();
				System.out.println("Nombre Instituto: " + nombreInstit); 
				String nombreCurso 		= (String) combocursos.getSelectedItem();
				System.out.println("Nombre Curso: " + nombreCurso);
				String nombreEdicion 	= (String) textAreaNombreEdicion.getText();
				System.out.println("Nombre Edicion: " + nombreEdicion);
				// para la fecha
				// obtengo la fecha de dateChooser FECHA INICIAL
				int anioI = dateChooserInicio.getCalendar().get(Calendar.YEAR);
				int mesI = dateChooserInicio.getCalendar().get(Calendar.MARCH) + 1;
				int diaI = dateChooserInicio.getCalendar().get(Calendar.DAY_OF_MONTH);
			    String fechaIString = diaI+"-"+mesI+"-"+anioI;  
			    						
				DateFormat formatoFecha = new SimpleDateFormat("dd-MM-yyyy");
				Date dateFechaI = null;
				try {
					dateFechaI = formatoFecha.parse(fechaIString);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//System.out.println(fechaI); formato date
				// reformato a como quiero que salga 01-12-1992
				
			    String fechaI= formatoFecha.format(dateFechaI);  
			    System.out.println("Fecha Inicio: " + dateFechaI); 

				// obtengo la fecha de dateChooser FECHA FINAL
				int anioF = dateChooserFin.getCalendar().get(Calendar.YEAR);
				int mesF = dateChooserFin.getCalendar().get(Calendar.MARCH) + 1;
				int diaF = dateChooserFin.getCalendar().get(Calendar.DAY_OF_MONTH);
			    String fechaFString = diaF+"-"+mesF+"-"+anioF;  
			    						
				Date dateFechaF = null;
				try {
					dateFechaF = formatoFecha.parse(fechaFString);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//System.out.println(fechaI); formato date
				// reformato a como quiero que salga 01-12-1992
				
			    String fechaF= formatoFecha.format(dateFechaF);  
			    System.out.println("Fecha Fin: " + dateFechaF);		
			    
			 // obtengo la fecha de dateChooser FECHA PUBLICACION
				int anioP = dateChooserPublicacion.getCalendar().get(Calendar.YEAR);
				int mesP = dateChooserPublicacion.getCalendar().get(Calendar.MARCH) + 1;
				int diaP = dateChooserPublicacion.getCalendar().get(Calendar.DAY_OF_MONTH);
			    String fechaPString = diaP+"-"+mesP+"-"+anioP;  
			    						
				Date dateFechaP = null;
				try {
					dateFechaP = formatoFecha.parse(fechaPString);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//System.out.println(fechaI); formato date
				// reformato a como quiero que salga 01-12-1992
				
			    String fechaP= formatoFecha.format(dateFechaP);  
			    System.out.println("Fecha Publicacion: " + dateFechaP); 
			    
			    int cupo = Integer.parseInt(txtAreaCupo.getText());
			    System.out.println("Cupo: " + cupo); 
				
			    System.out.println("Cantidad Docentes: " + listaDocentesParaAgregar.size()); 
							   
			    boolean exito = iconIP.AltaEdicionCurso(nombreInstit, nombreCurso, nombreEdicion, dateFechaI, dateFechaF, cupo, dateFechaP, listaDocentesParaAgregar);
			    if (exito) {
					System.out.println("Se dio de alta la edicion "+nombreEdicion);
				} else {
					System.out.println("El nombre de edicion "+nombreEdicion+" ya existe.");
					Object[] options1 = {"Modificar Datos"};
					textAreaNombreEdicion.setText("");

					JOptionPane.showOptionDialog(null,
					                 "La edicion " + nombreEdicion + " ya existe por favor edite los datos.",
					                 "Alta Edicion Curso",
					                 JOptionPane.YES_NO_CANCEL_OPTION,
					                 JOptionPane.PLAIN_MESSAGE,
					                 null,
					                 options1,
					                 null);
				}
			    
			    			   
			}
		});
		btnDarAltaEdicion.setBounds(57, 427, 525, 25);
		getContentPane().add(btnDarAltaEdicion);
		
	}
	
	private void updateTextTareaDocentes(List<String> listaDocentes){		
		JTextArea textAreaDocentes = new JTextArea();
		textAreaDocentes.setBounds(399, 142, 183, 272);
		getContentPane().add(textAreaDocentes);
		textAreaDocentes.setEditable(false);
		
		if(!listaDocentes.isEmpty()) {
			for(String i : listaDocentes){
				System.out.println(i);
				textAreaDocentes.append(i+"\n");
			}					
		} else {
			textAreaDocentes.append("No se han agregado Docentes\n");	
		}				
	}
	
	
	
}
