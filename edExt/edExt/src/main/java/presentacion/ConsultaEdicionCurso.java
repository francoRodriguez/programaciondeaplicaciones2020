package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

import datatypes.DtEdicion;
import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;
import logica.Docente;
import logica.Usuario;

import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Color;

@SuppressWarnings("unused")
public class ConsultaEdicionCurso extends JInternalFrame {
	
	private static final long serialVersionUID = 1L;
	private IControladorUsuario iconUs;
	private IControladorInstProg IconIP;
	
	private JComboBox<String> comboBoxListarInstitutos;
	private JComboBox<String> comboBoxListarCursos;
	private JComboBox<String> comboBoxListarEdiciones;
	
	/**
	 * Launch the application.
	/**
	 * Create the frame.
	 */
	@SuppressWarnings("rawtypes")
	public ConsultaEdicionCurso(final IControladorInstProg iconIP, final IControladorUsuario iconU) {
		setTitle("Consulta Edicion Curso");
		setClosable(true);
		setBounds(100, 100, 627, 447);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Consulta Edicion Curso");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel.setBounds(12, 13, 410, 25);
		getContentPane().add(lblNewLabel);
		
		comboBoxListarInstitutos = new JComboBox<String>();
		comboBoxListarInstitutos.setBounds(228, 70, 194, 22);
		getContentPane().add(comboBoxListarInstitutos);		
		
		final JComboBox comboBoxListarCursos = new JComboBox<String>();
		comboBoxListarCursos.setBounds(228, 107, 194, 22);
		getContentPane().add(comboBoxListarCursos);
		
		JComboBox comboBoxListarEdiciones = new JComboBox<String>();
		comboBoxListarEdiciones.setBounds(228, 144, 194, 22);
		getContentPane().add(comboBoxListarEdiciones);
		
		JLabel lblInstituto = new JLabel("Seleccione el Instituto:");
		lblInstituto.setBounds(12, 73, 161, 16);
		getContentPane().add(lblInstituto);
		
		JLabel lblCurso = new JLabel("Seleccione el Curso:");
		lblCurso.setBounds(12, 110, 197, 16);
		getContentPane().add(lblCurso);	
		
		JLabel lblEdicion = new JLabel("Seleccione la Edicion:");
		lblEdicion.setBounds(12, 147, 197, 16);
		getContentPane().add(lblEdicion);	
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(12, 193, 587, 177);
		getContentPane().add(textArea);
				
		JButton btnSeleccionarInstituto = new JButton("Seleccionar");
		btnSeleccionarInstituto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String instituto = (String) comboBoxListarInstitutos.getSelectedItem();
				
				System.out.println("SeleccionoInstituto: " + instituto);
				
				// Borro todos los datos cargados cada vez que haga clic
				comboBoxListarCursos.removeAllItems();
				
				//Busco la lista de cursos para el instituto elegido
				String[] listaCursos = iconIP.listarNombreCursos(instituto);
				// Si hay registros 
				System.out.println("Cantidad de cursos:  " + listaCursos.length);
				if(listaCursos.length > 0) {
					String primerItem = listaCursos[0];
					for(String i : listaCursos){
						System.out.println(i);
						comboBoxListarCursos.addItem(i);					
					}			
					comboBoxListarCursos.setSelectedItem(primerItem);
					getContentPane().add(comboBoxListarCursos);				

					// Pongo el comboBox como habilitado
					comboBoxListarCursos.setEnabled(true);
				} else {
					//Si no hay registros el combo box deshabilitado y con este mensaje
					comboBoxListarCursos.addItem("No hay datos para mostrar.");	
					comboBoxListarCursos.setEnabled(false);
				}
				
			}
		});
		btnSeleccionarInstituto.setBounds(434, 69, 161, 25);
		getContentPane().add(btnSeleccionarInstituto);
		
		JButton btnSeleccionarCurso = new JButton("Seleccionar");
		btnSeleccionarCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String curso = (String) comboBoxListarCursos.getSelectedItem();
				
				System.out.println("SeleccionoCurso: " + curso);				
				//Busco la lista de cursos para el instituto elegido
				String[] listaEdicioes = iconIP.listarEdicionesdeCurso(curso);
				// Si hay registros 
				System.out.println("Cantidad de cursos:  " + listaEdicioes.length);
				if(listaEdicioes.length > 0) {
					String primerItem = listaEdicioes[0];
					for(String i : listaEdicioes){
						System.out.println(i);
						comboBoxListarEdiciones.addItem(i);					
					}			
					comboBoxListarEdiciones.setSelectedItem(primerItem);
					getContentPane().add(comboBoxListarEdiciones);				

					// Pongo el comboBox como habilitado
					comboBoxListarEdiciones.setEnabled(true);
				} else {
					//Si no hay registros el combo box deshabilitado y con este mensaje
					comboBoxListarEdiciones.addItem("No hay datos para mostrar.");	
					comboBoxListarEdiciones.setEnabled(false);
				}
				
			}
		});
		btnSeleccionarCurso.setBounds(434, 106, 161, 25);
		getContentPane().add(btnSeleccionarCurso);
		
		comboBoxListarInstitutos.setEnabled(false);
		JButton btnInstitutos = new JButton(",");
		btnInstitutos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(comboBoxListarInstitutos.isEnabled()==false) {
					// Inicialiar Comobo Box
					comboBoxListarInstitutos.setEnabled(true);
					comboBoxListarInstitutos.removeAllItems();
					String[] lista = iconIP.listarInstitutosArray();					
					// Si hay registros 
					if(lista != null) {
						String primerItem = lista[0];
						for(String i : lista){
							System.out.println(i);
							comboBoxListarInstitutos.addItem(i);					
						}			
						comboBoxListarInstitutos.setSelectedItem(primerItem);
						getContentPane().add(comboBoxListarInstitutos);
						
					} else {
						comboBoxListarInstitutos.addItem("Sin Datos");	
						comboBoxListarInstitutos.setEnabled(false);
					}
				}
				
				
			}
		});
		btnInstitutos.setForeground(Color.WHITE);
		btnInstitutos.setIcon(new ImageIcon(ConsultaCurso.class.getResource("/img/Recargar.png")));
		btnInstitutos.setBounds(10, 42, 37, 27);
		getContentPane().add(btnInstitutos);
		
		JButton btnMostrarDatos = new JButton("Mostrar Datos");
		btnMostrarDatos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.setText("Datos de Edicion de Curso\n");
				DtEdicion dt = iconIP.mostrarEdicion((String)comboBoxListarEdiciones.getSelectedItem());
				textArea.append("Nombre: " + dt.getNombre() + "\n");
				textArea.append("Fecha de Inicio: " + dt.getFechaI() + "\n");
				textArea.append("Fecha de Finalizacion: " + dt.getFechaF() + "\n");
				textArea.append("Cupo: " + dt.getCupo() + "\n") ;
				textArea.append("Fecha de Publicacion: " + dt.getFechaPub() + "\n");
				textArea.append("\nLista de Docentes:\n");
				
				List<Docente> lista = dt.getListaDocentes();
				System.out.println("Cantidad de Docentes: " + lista.size());
				for (Docente l : lista) {
					System.out.println(l.getNickName());	
					textArea.append(l.getNickName() + "\n");
				}				
			}
		});
		btnMostrarDatos.setBounds(434, 143, 161, 25);
		getContentPane().add(btnMostrarDatos);
		


	}
}
