package presentacion;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.toedter.calendar.JDateChooser;

import excepciones.EmailExistenteException;
import excepciones.NickNameExistenteException;
import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;
import logica.Curso;
import logica.Docente;
import logica.Edicion;
import logica.Instituto;
import logica.Usuario;
import persistencia.Conexion;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;

public class ModificarUsuario extends JInternalFrame {
	private static final long serialVersionUID = 1L;
	
	private IControladorUsuario iConUsu;
	
	private JComboBox<String> comboBoxUsuario; 
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtCorreo;
	private JDateChooser dateChooserFN;
	private JButton btnAceptar;

	/**
	 * Launch the application.
	 *//*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConsultaUsuario frame = new ConsultaUsuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 * @param iConUsu 
	 */
	public ModificarUsuario(final IControladorUsuario iConUsu) {
		this.iConUsu = iConUsu;
		setTitle("Modificar Usuario");
		setClosable(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblTitulo = new JLabel("Modificar Usuario");
		lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitulo.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblTitulo.setBounds(12, 13, 410, 25);
		getContentPane().add(lblTitulo);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(12, 54, 56, 16);
		getContentPane().add(lblUsuario);
		
		comboBoxUsuario = new JComboBox<String>();
		comboBoxUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rellenarDatos();
			}
		});
		comboBoxUsuario.setBounds(75, 51, 149, 22);
		getContentPane().add(comboBoxUsuario);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(12, 83, 56, 16);
		getContentPane().add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(12, 121, 56, 16);
		getContentPane().add(lblApellido);
		
		JLabel lblCorreo = new JLabel("Correo");
		lblCorreo.setBounds(12, 156, 56, 16);
		getContentPane().add(lblCorreo);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(75, 83, 149, 22);
		getContentPane().add(txtNombre);
		txtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setBounds(75, 118, 149, 22);
		getContentPane().add(txtApellido);
		txtApellido.setColumns(10);
		
		txtCorreo = new JTextField();
		txtCorreo.setEditable(false);
		txtCorreo.setBounds(75, 153, 149, 22);
		getContentPane().add(txtCorreo);
		txtCorreo.setColumns(10);
		
		JLabel lblFechaDeNacimiento = new JLabel("Fecha de nacimiento");
		lblFechaDeNacimiento.setBounds(12, 192, 177, 15);
		getContentPane().add(lblFechaDeNacimiento);
		
		dateChooserFN = new JDateChooser();
		dateChooserFN.setBounds(166, 188, 162, 19);
		getContentPane().add(dateChooserFN);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cambiarDatos();
			}
		});
		btnAceptar.setBounds(277, 226, 97, 25);
		getContentPane().add(btnAceptar);
	}
	
	public void inicializarComboBoxes() {
		DefaultComboBoxModel<String> nickUsuarios = new DefaultComboBoxModel<String>(iConUsu.listarUsuarios());
		comboBoxUsuario.setModel(nickUsuarios);
		
	}
	
	public void rellenarDatos() {
		Usuario u = iConUsu.seleccionarUsuario(this.comboBoxUsuario.getSelectedItem().toString());
		txtNombre.setText(u.getNombre());
		txtApellido.setText(u.getApellido());
		txtCorreo.setText(u.getCorreo());
		dateChooserFN.setDate(u.getFechaNacimiento());
	}
	
	public void cambiarDatos() {		
		Usuario u = iConUsu.seleccionarUsuario(this.comboBoxUsuario.getSelectedItem().toString());
		try {
			int anioN = dateChooserFN.getCalendar().get(Calendar.YEAR);
			int mesN = dateChooserFN.getCalendar().get(Calendar.MONTH) + 1;
			int diaN = dateChooserFN.getCalendar().get(Calendar.DAY_OF_MONTH);
		    String fechaNString = diaN+"-"+mesN+"-"+anioN;
	        DateFormat formatoFecha = new SimpleDateFormat("dd-MM-yyyy");
			Date fNac = null;
			try {
				fNac = formatoFecha.parse(fechaNString);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(!txtNombre.getText().isEmpty() && !txtApellido.getText().isEmpty())
			{
				iConUsu.modificarDatos(u.getNickName(), txtNombre.getText(), txtApellido.getText(), u.getCorreo(), dateChooserFN.getDate());
				JOptionPane.showMessageDialog(this, "El usuario ha sido modificado con exito", "Modificar Usuario", JOptionPane.INFORMATION_MESSAGE);
	            //limpiarFormulario();
			}
			else {
				JOptionPane.showMessageDialog(this, "No puedes tener campos vacios crack.", "Modificar Usuario", JOptionPane.ERROR_MESSAGE);
			}
		}
		catch(Exception e) {
			JOptionPane.showMessageDialog(this, "Pon bien la fecha co�o!", "Modificar Usuario", JOptionPane.ERROR_MESSAGE);
		}
	}
}	